import pytest
from bliss.controllers.lima import properties


@pytest.mark.parametrize(
    "base_class,",
    (
        properties.LimaAttributesAsProperties,
        properties.LimaAttributesAsDeferredWriteProperties,
    ),
)
@pytest.mark.parametrize("tango_names", (True, False))
@pytest.mark.parametrize("writeable_only", (True, False))
def test_lima_properties_to_dict(
    base_class, tango_names, writeable_only, beacon, lima_simulator
):
    simulator = beacon.get("lima_simulator")
    deferred = base_class is properties.LimaAttributesAsDeferredWriteProperties

    class MyInterface(
        base_class, proxy=simulator.proxy, prefix="acc_", strip_prefix=False
    ):
        pass

    class MyInterfaceStripped(
        base_class, proxy=simulator.proxy, prefix="acc_", strip_prefix=True
    ):
        pass

    if deferred:
        proxy = MyInterface({"name": "name1"})
        proxy_stripped = MyInterfaceStripped({"name": "name2"})
    else:
        proxy = MyInterface()
        proxy_stripped = MyInterfaceStripped()

    kw = {"tango_names": tango_names, "writeable_only": writeable_only}
    adict = proxy.to_dict(**kw)
    adict_stripped = proxy_stripped.to_dict(**kw)
    if not tango_names:
        adict_stripped = {"acc_" + k: v for k, v in adict_stripped.items()}
    assert adict == adict_stripped


def test_lima_property_values(beacon, lima_simulator):
    simulator = beacon.get("lima_simulator")

    class MyInterface(
        properties.LimaAttributesAsProperties,
        proxy=simulator.proxy,
        prefix="acc_",
        strip_prefix=True,
    ):
        pass

    proxy = MyInterface()

    # Check writing to a read-only attribute
    expo_time = simulator.proxy.acc_expo_time
    with pytest.raises(Exception):
        simulator.proxy.acc_expo_time = expo_time + 1
    with pytest.raises(Exception):
        proxy.expo_time = expo_time + 1
    assert proxy.expo_time == expo_time
    assert proxy.expo_time == expo_time

    # Check writing to a writable attribute
    max_expo_time = simulator.proxy.acc_max_expo_time
    try:
        simulator.proxy.acc_max_expo_time = max_expo_time + 1
        assert simulator.proxy.acc_max_expo_time == max_expo_time + 1
        assert proxy.max_expo_time == max_expo_time + 1

        proxy.max_expo_time = max_expo_time + 2
        assert proxy.max_expo_time == max_expo_time + 2
        assert simulator.proxy.acc_max_expo_time == max_expo_time + 2
    finally:
        simulator.proxy.acc_max_expo_time = max_expo_time


def test_lima_deferred_property_values(beacon, lima_simulator):
    simulator = beacon.get("lima_simulator")

    class MyInterface(
        properties.LimaAttributesAsDeferredWriteProperties,
        proxy=simulator.proxy,
        prefix="acc_",
        strip_prefix=True,
    ):
        pass

    proxy = MyInterface({"name": "name"})

    # Check writing to a read-only attribute
    expo_time = simulator.proxy.acc_expo_time
    with pytest.raises(Exception):
        simulator.proxy.acc_expo_time = expo_time + 1
    with pytest.raises(Exception):
        proxy.expo_time = expo_time + 2
    assert simulator.proxy.acc_expo_time == expo_time
    assert proxy.expo_time == expo_time
    proxy.apply(simulator.proxy)
    assert simulator.proxy.acc_expo_time == expo_time
    assert proxy.expo_time == expo_time

    # Check writing to a writable attribute
    max_expo_time = simulator.proxy.acc_max_expo_time
    try:
        # Modify in Lima
        simulator.proxy.acc_max_expo_time = max_expo_time + 1
        assert simulator.proxy.acc_max_expo_time == max_expo_time + 1
        assert proxy.max_expo_time == max_expo_time  # needs sync
        proxy.store(simulator.proxy)  # sync
        assert proxy.max_expo_time == max_expo_time + 1

        # Modify in buffer
        proxy.max_expo_time = max_expo_time + 2
        assert proxy.max_expo_time == max_expo_time + 2
        assert simulator.proxy.acc_max_expo_time == max_expo_time + 1  # needs sync
        proxy.apply(simulator.proxy)  # sync
        assert simulator.proxy.acc_max_expo_time == max_expo_time + 2
    finally:
        simulator.proxy.acc_max_expo_time = max_expo_time


def test_lima_properties_info(beacon, lima_simulator):
    simulator = beacon.get("lima_simulator")

    class MyInterface(
        properties.LimaAttributesAsProperties,
        proxy=simulator.proxy,
        prefix="acc_",
        strip_prefix=True,
    ):
        pass

    proxy = MyInterface()
    info = dict([line.split(": ") for line in proxy.__info__().split("\n")])
    for name, str_value in info.items():
        expected_value = getattr(simulator.proxy, "acc_" + name)
        if str_value.startswith("|"):
            str_value = str_value[1:-1]
        assert str_value == repr(expected_value)


def test_lima_properties_enum(beacon, lima_simulator):
    simulator = beacon.get("lima_simulator")

    acc_modes = simulator._proxy.getAttrStringValueList("acc_mode")
    assert all(mode in dir(simulator.accumulation.mode_enum) for mode in acc_modes)


def test_to_dict_issue_3019(beacon, lima_simulator):
    simulator = beacon.get("lima_simulator")

    d = simulator.camera.to_dict()
    assert "test" in d  # LimaProperty
    assert "test2" not in d  # normal property

    d2 = simulator.camera.to_dict(exclude_properties=["test"])
    assert "test" not in d2
    d.pop("test")
    assert str(d) == str(d2)

    with pytest.raises(KeyError):
        d = simulator.camera.to_dict(include_properties=["test2"])
