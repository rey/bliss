# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
import gevent
import tempfile
import subprocess
import sys
import os

from bliss.common.shutter import BaseShutter, BaseShutterState, Shutter
from bliss.comm.rpc import Client
from bliss.common import event
from .sim_shutter_test import get_simulation_shutter


@pytest.fixture
def simulation_shutter():
    return get_simulation_shutter()


@pytest.fixture
def simulation_shutter_in_another_process(beacon, beacon_host_port):
    p = subprocess.Popen(
        [
            sys.executable,
            "-u",
            os.path.join(os.path.dirname(__file__), "sim_shutter_test.py"),
            f"{beacon_host_port[0]}",
            f"{beacon_host_port[1]}",
        ],
        stdout=subprocess.PIPE,
    )
    line = p.stdout.readline()  # synchronize process start
    try:
        port = int(line)
    except ValueError:
        raise RuntimeError("server didn't start")

    shutter_in_another_process_proxy = Client(f"tcp://localhost:{port}")

    yield p, shutter_in_another_process_proxy

    p.terminate()
    shutter_in_another_process_proxy._rpc_connection.close()


def test_simulation_shutter(
    beacon, simulation_shutter, simulation_shutter_in_another_process
):
    _, external_simulation_shutter = simulation_shutter_in_another_process
    assert external_simulation_shutter.state_string == simulation_shutter.state_string
    assert simulation_shutter.state_string == "Closed"

    simulation_shutter.open()

    assert simulation_shutter.state_string == "Open"
    assert external_simulation_shutter.state_string == simulation_shutter.state_string

    cb_called_event = gevent.event.AsyncResult()

    def state_changed_event(state):
        cb_called_event.set(state)

    event.connect(simulation_shutter, "state", state_changed_event)

    # close shutter from the second process, and ensure we got called via state change event
    try:
        external_simulation_shutter.close()
        with gevent.Timeout(3):
            cb_called_event.wait()
        assert cb_called_event.value == BaseShutterState.CLOSED
    finally:
        event.disconnect(simulation_shutter, "state", state_changed_event)
