def test_spectrometer_init(beacon):
    s = beacon.get("spectro")
    s.energy_axis.move(11.2)
    s.energy_axis.move(11.3)

    s._calc_mot.close()
    s._ene_calc.close()
    s._disconnect_axes()
