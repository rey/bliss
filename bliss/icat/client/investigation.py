import os
from datetime import datetime
from typing import Optional, List
from urllib.parse import urljoin
import requests
import numpy

from bliss.icat.client.url import normalize_url
from bliss.icat.client.interface import DatasetId


def arg_smallest_positive(arr: numpy.ndarray) -> Optional[int]:
    condition = arr >= 0
    if condition.any():
        return numpy.where(condition, arr, numpy.inf).argmin()


class IcatInvestigationClient:
    """Client for the investigation part of the ICAT+ REST API.

    An "investigation" is a time slot assigned to a particular proposal
    at a particular beamline.

    REST API docs:
    https://icatplus.esrf.fr/api-docs/

    The ICAT+ server project:
    https://gitlab.esrf.fr/icat/icat-plus/-/blob/master/README.md
    """

    DEFAULT_SCHEME = "https"

    def __init__(
        self,
        url: str,
        api_key: str = "elogbook-00000000-0000-0000-0000-000000000000",
        timeout: Optional[float] = None,
    ):
        url = normalize_url(url, default_scheme=self.DEFAULT_SCHEME)

        path = f"dataacquisition/{api_key}/investigation"
        query = (
            "?investigationName={proposal}&instrumentName={beamline}&sortBy=startdate"
        )
        self._investigation_url = urljoin(url, path + query)

        path = f"dataacquisition/{api_key}/dataset"
        query = "?investigationId={investigation_id}"
        self._dataset_url = urljoin(url, path + query)

        self.raise_error = False
        if timeout is None:
            timeout = 2
        self.timeout = timeout
        self.__investigation_info = dict()

    def _get_with_response_parsing(self, url: str) -> Optional[list]:
        """Return `None` means the information is not available at this moment.
        An empty list means that an error has occured or an actual empty list
        is returned.
        """
        try:
            response = requests.get(url, timeout=self.timeout)
        except requests.exceptions.ReadTimeout:
            return None
        if self.raise_error:
            response.raise_for_status()
        if response.ok:
            return response.json()
        else:
            return list()

    def investigation_info(self, proposal: str, beamline: str) -> Optional[dict]:
        investigation_key = proposal, beamline
        ninfo = self.__investigation_info.get(investigation_key)
        if ninfo is not None:
            return ninfo

        # Get all investigations for this proposal and beamline
        url = self._investigation_url.format(proposal=proposal, beamline=beamline)
        investigations = self._get_with_response_parsing(url)
        if investigations is None:
            return None  # not available at the moment
        if not investigations:
            return dict()  # error or no investigation found

        # Get the closest investigation which started before "now".
        # If there is no such investigation, get the closest investigation
        # which starts after "time".
        now = datetime.now().astimezone()
        seconds_since_start = numpy.array(
            [
                (now - datetime.fromisoformat(info["startDate"])).total_seconds()
                for info in investigations
            ]
        )
        idx = arg_smallest_positive(seconds_since_start)
        if idx is None:
            idx = arg_smallest_positive(-seconds_since_start)
        info = investigations[idx]

        # Normalize information
        for key in ["parameters", "visitId"]:
            info.pop(key, None)
        ninfo = dict()
        ninfo["proposal"] = info.pop("name", None)
        ninfo["beamline"] = info.pop("instrument", dict()).get("name", None)
        ninfo.update(info)
        ninfo["e-logbook"] = f"https://data.esrf.fr/investigation/{info['id']}/events"
        ninfo[
            "data portal"
        ] = f"https://data.esrf.fr/investigation/{info['id']}/datasets"

        self.__investigation_info[investigation_key] = ninfo
        return ninfo

    def _investigation_id(self, proposal: str, beamline: str) -> Optional[int]:
        info = self.investigation_info(proposal, beamline)
        if info is None:
            return None
        return info.get("id", None)

    def registered_dataset_ids(
        self, proposal: str, beamline: str
    ) -> Optional[List[DatasetId]]:
        investigation_id = self._investigation_id(proposal, beamline)
        if investigation_id is None:
            return None
        url = self._dataset_url.format(investigation_id=investigation_id)
        datasets = self._get_with_response_parsing(url)
        if datasets is None:
            return None
        return [self._icat_dataset_to_datasetid(dataset) for dataset in datasets]

    @staticmethod
    def _icat_dataset_to_datasetid(dataset: dict) -> DatasetId:
        location = dataset["location"]
        location, redis_name = os.path.split(location)
        while location and not redis_name:
            location, redis_name = os.path.split(location)
        return DatasetId(redis_name=redis_name, path=dataset["location"])
