from contextlib import contextmanager
from typing import Iterable, Optional, List, Tuple
import numpy

from bliss.icat.client.interface import IcatClientInterface
from bliss.icat.client.interface import DatasetId
from bliss.icat.client.elogbook import IcatElogbookClient
from bliss.icat.client.metadata import IcatMetadataClient
from bliss.icat.client.investigation import IcatInvestigationClient
from bliss import current_session


class IcatClient(IcatClientInterface):
    """Direct communication with ICAT: e-logbook and metadata

    All constructor arguments can be provided in the beamline configuration under `icat_servers`:

        icat_servers:
            metadata_urls: [URL1, URL2]
            elogbook_url: URL3
            elogbook_token: elogbook-00000000-0000-0000-0000-000000000000
    """

    def __init__(
        self,
        metadata_urls: list,
        elogbook_url: str,
        elogbook_token: str = "elogbook-00000000-0000-0000-0000-000000000000",
        metadata_queue: str = "icatIngest",
        metadata_queue_monitor_port: Optional[int] = None,
        elogbook_timeout: Optional[float] = None,
        feedback_timeout: Optional[float] = None,
        queue_timeout: Optional[float] = None,
        proposal: Optional[str] = None,
        beamline: Optional[str] = None,
    ):
        self.current_proposal = proposal
        self.current_beamline = beamline
        self.elogbook_client = IcatElogbookClient(
            url=elogbook_url, api_key=elogbook_token, timeout=elogbook_timeout
        )
        self.metadata_client = IcatMetadataClient(
            queue_urls=metadata_urls,
            queue_name=metadata_queue,
            monitor_port=metadata_queue_monitor_port,
            timeout=queue_timeout,
        )
        self.investigation_client = IcatInvestigationClient(
            url=elogbook_url, api_key=elogbook_token, timeout=feedback_timeout
        )

    @property
    def current_proposal(self):
        if self.__current_proposal is None or current_session:
            self.__current_proposal = current_session.scan_saving.proposal_name
        return self.__current_proposal

    @current_proposal.setter
    def current_proposal(self, value: Optional[str]):
        self.__current_proposal = value

    @property
    def current_beamline(self):
        if self.__current_beamline is None or current_session:
            self.__current_beamline = current_session.scan_saving.beamline
        return self.__current_beamline

    @current_beamline.setter
    def current_beamline(self, value: Optional[str]):
        self.__current_beamline = value

    @property
    def current_dataset(self):
        return current_session.scan_saving.dataset_name

    @property
    def current_dataset_metadata(self):
        return current_session.scan_saving.dataset.get_current_icat_metadata()

    @property
    def current_collection(self):
        return current_session.scan_saving.collection

    @property
    def current_path(self):
        return current_session.scan_saving.icat_root_path

    def send_message(
        self,
        msg: str,
        msg_type="comment",
        proposal: Optional[str] = None,
        beamline: Optional[str] = None,
        dataset: Optional[str] = None,
    ):
        if proposal is None:
            proposal = self.current_proposal
        if beamline is None:
            beamline = self.current_beamline
        if dataset is None:
            dataset = self.current_dataset
        self.elogbook_client.send_message(
            message=msg,
            category=msg_type,
            proposal=proposal,
            beamline=beamline,
            dataset=dataset,
        )

    def send_binary_data(
        self,
        data: bytes,
        mimetype: Optional[str] = None,
        proposal: Optional[str] = None,
        beamline: Optional[str] = None,
    ):
        if proposal is None:
            proposal = self.current_proposal
        if beamline is None:
            beamline = self.current_beamline
        self.elogbook_client.send_binary_data(
            data, mimetype=mimetype, proposal=proposal, beamline=beamline
        )

    def send_text_file(
        self,
        filename: str,
        proposal: Optional[str] = None,
        beamline: Optional[str] = None,
        dataset: Optional[str] = None,
    ):
        if proposal is None:
            proposal = self.current_proposal
        if beamline is None:
            beamline = self.current_beamline
        if dataset is None:
            dataset = self.current_dataset
        self.elogbook_client.send_text_file(
            filename, proposal=proposal, beamline=beamline, dataset=dataset
        )

    def send_binary_file(
        self,
        filename: str,
        proposal: Optional[str] = None,
        beamline: Optional[str] = None,
    ):
        if proposal is None:
            proposal = self.current_proposal
        if beamline is None:
            beamline = self.current_beamline
        self.elogbook_client.send_binary_file(
            filename, proposal=proposal, beamline=beamline
        )

    def start_investigation(
        self,
        proposal: Optional[str] = None,
        beamline: Optional[str] = None,
        start_datetime=None,
    ):
        if proposal is None:
            proposal = self.current_proposal
        else:
            self.current_proposal = proposal
        if beamline is None:
            beamline = self.current_beamline
        else:
            self.current_beamline = beamline
        self.metadata_client.start_investigation(
            proposal=proposal, beamline=beamline, start_datetime=start_datetime
        )

    def store_dataset(
        self,
        proposal: Optional[str] = None,
        beamline: Optional[str] = None,
        collection: Optional[str] = None,
        dataset: Optional[str] = None,
        path: Optional[str] = None,
        metadata: dict = None,
        start_datetime=None,
        end_datetime=None,
    ):
        if proposal is None:
            proposal = self.current_proposal
        if beamline is None:
            beamline = self.current_beamline
        if collection is None:
            collection = self.current_collection
        if dataset is None:
            dataset = self.current_dataset
        if path is None:
            path = self.current_path
        if metadata is None:
            metadata = self.current_dataset_metadata
        self.metadata_client.send_metadata(
            proposal=proposal,
            beamline=beamline,
            collection=collection,
            dataset=dataset,
            path=path,
            metadata=metadata,
            start_datetime=start_datetime,
            end_datetime=end_datetime,
        )

    def investigation_info(self, proposal: str, beamline: str) -> Optional[dict]:
        return self.investigation_client.investigation_info(proposal, beamline)

    def registered_dataset_ids(
        self, proposal: str, beamline: str
    ) -> Optional[List[DatasetId]]:
        return self.investigation_client.registered_dataset_ids(proposal, beamline)

    def investigation_info_string(self, proposal: str, beamline: str) -> str:
        info = self.investigation_info(proposal, beamline)
        if info:
            rows = [(str(k), str(v)) for k, v in info.items()]
            lengths = numpy.array([[len(s) for s in row] for row in rows])
            fmt = "   ".join(["{{:<{}}}".format(n) for n in lengths.max(axis=0)])
            infostr = "ICAT proposal time slot:\n "
            infostr += "\n ".join([fmt.format(*row) for row in rows])
        elif info is None:
            infostr = f"Proposal information currently not available ({self.reason_for_missing_information})"
        else:
            infostr = "Proposal NOT available in the data portal"
        return infostr

    def investigation_summary(self, proposal: str, beamline: str) -> List[Tuple]:
        info = self.investigation_info(proposal, beamline)
        keys = ["e-logbook", "data portal"]
        if info:
            rows = [(key, info[key]) for key in keys]
        elif info is None:
            rows = [
                (
                    key,
                    f"Proposal information currently not available ({self.reason_for_missing_information})",
                )
                for key in keys
            ]
        else:
            rows = [(key, "Proposal NOT available in the data portal") for key in keys]
        return rows

    @property
    def expire_datasets_on_close(self) -> bool:
        return False

    @property
    def reason_for_missing_information(self) -> str:
        return "ICAT communication timeout"

    @contextmanager
    def timeout_context(self, feedback_timeout=None) -> Iterable[None]:
        if feedback_timeout is not None:
            keep = self.investigation_client.timeout
            self.investigation_client.timeout = feedback_timeout
        try:
            yield
        finally:
            if feedback_timeout is not None:
                self.investigation_client.timeout = keep

    def ping(self, *args, **kw):
        """For compatibility with IcatTangoProxy
        """
        pass
