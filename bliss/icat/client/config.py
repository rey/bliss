import logging
from typing import Union, Optional
from bliss.config.static import get_config
from bliss.icat.client.main import IcatClient
from bliss.icat.client.tango import IcatTangoProxy
from bliss.icat.client.null import IcatNullClient


logger = logging.getLogger(__name__)


def icat_client_config(
    bliss_session: str, beamline: str, proposal: Optional[str] = None
):
    config = get_config().root.get("icat_servers")
    if config:
        if proposal is not None:
            config["proposal"] = proposal
        config["beamline"] = beamline
        disable = config.pop("disable", False)
        return {"tango": False, "disable": disable, "kwargs": config}
    else:
        return {
            "tango": True,
            "disable": False,
            "kwargs": {"session": bliss_session, "beamline": beamline},
        }


def icat_client_from_config(config: dict):
    if config["disable"]:
        return IcatNullClient(expire_datasets_on_close=False)
    elif config["tango"]:
        client = IcatTangoProxy(**config["kwargs"], expire_datasets_on_close=True)
        if client.tango_devices_exist:
            return client
        return IcatNullClient(expire_datasets_on_close=True)
    else:
        return IcatClient(**config["kwargs"])


def is_tango_client(icat_client: Union[IcatClient, IcatTangoProxy, IcatNullClient]):
    return isinstance(icat_client, IcatTangoProxy)


def is_null_client(icat_client: Union[IcatClient, IcatTangoProxy, IcatNullClient]):
    return isinstance(icat_client, IcatNullClient)
