from contextlib import contextmanager
from typing import Iterable, Optional, List, Tuple
from dataclasses import dataclass


@dataclass(frozen=True)
class DatasetId:
    redis_name: str
    path: str


class IcatClientInterface:
    def send_message(
        self,
        msg: str,
        msg_type="comment",
        proposal: Optional[str] = None,
        beamline: Optional[str] = None,
        dataset: Optional[str] = None,
    ):
        raise NotImplementedError

    def send_data(
        self,
        data: bytes,
        mimetype: Optional[str] = None,
        proposal: Optional[str] = None,
        beamline: Optional[str] = None,
    ):
        raise NotImplementedError

    def send_text_file(
        self,
        filename: str,
        proposal: Optional[str] = None,
        beamline: Optional[str] = None,
        dataset: Optional[str] = None,
    ):
        raise NotImplementedError

    def send_binary_file(
        self,
        filename: str,
        proposal: Optional[str] = None,
        beamline: Optional[str] = None,
    ):
        raise NotImplementedError

    def start_investigation(
        self,
        proposal: Optional[str] = None,
        beamline: Optional[str] = None,
        start_datetime=None,
    ):
        raise NotImplementedError

    def store_dataset(
        self,
        proposal: Optional[str] = None,
        beamline: Optional[str] = None,
        collection: Optional[str] = None,
        dataset: Optional[str] = None,
        path: Optional[str] = None,
        metadata: dict = None,
        start_datetime=None,
        end_datetime=None,
    ):
        raise NotImplementedError

    def investigation_info(self, proposal: str, beamline: str) -> Optional[dict]:
        raise NotImplementedError

    def registered_dataset_ids(
        self, proposal: str, beamline: str
    ) -> Optional[List[DatasetId]]:
        raise NotImplementedError

    def investigation_info_string(self, proposal: str, beamline: str) -> str:
        raise NotImplementedError

    def investigation_summary(self, proposal: str, beamline: str) -> List[Tuple]:
        raise NotImplementedError

    @property
    def expire_datasets_on_close(self) -> bool:
        raise NotImplementedError

    @property
    def reason_for_missing_information(self) -> str:
        raise NotImplementedError

    @contextmanager
    def timeout_context(self, feedback_timeout=None) -> Iterable[None]:
        raise NotImplementedError
