# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.


import os
import datetime
from bliss import current_session
from bliss.icat import FieldGroup
from bliss.common.utils import autocomplete_property
from bliss.icat.policy import DataPolicyObject
from bliss.icat.dataset_collection import DatasetCollection


class Dataset(DataPolicyObject):
    """A dataset can be

     * closed: metadata is gathered and frozen which means metadata cannot be
               changed or added and parent metadata is not inherited
     * registered: metadata is registered with ICAT

    A registered dataset is always closed but a closed dataset is not necessarily registered.
    """

    _REQUIRED_INFO = DataPolicyObject._REQUIRED_INFO | {"__closed__", "__registered__"}
    _NODE_TYPE = "dataset"

    def __init__(self, node):
        super().__init__(node)
        self._collection = None

    @property
    def expected_fields(self):
        """all fields required by this dataset"""
        all_fields = super().expected_fields
        # All technique fields are required
        for t in self.techniques:
            all_fields.update(t.fields)
        return all_fields

    def gather_metadata(self, on_exists=None):
        """Initialize the dataset node info.

        When metadata already exists in Redis:
            on_exists="skip": do nothing
            on_exists="overwrite": overwrite in Redis
            else: raise RuntimeError
        """
        if self.is_closed:
            raise RuntimeError("The dataset is already closed")

        if self.metadata_gathering_done:
            if on_exists == "skip":
                return
            elif on_exists == "overwrite":
                pass
            else:
                raise RuntimeError("Metadata gathering already done")

        # Gather metadata
        if current_session.icat_mapping:
            infodict = current_session.icat_mapping.get_metadata()
        else:
            infodict = dict()

        # Check metadata
        for field_name, field_value in infodict.items():
            assert self.validate_fieldname(
                field_name
            ), f"{field_name} is not an accepted key in this dataset!"
            assert isinstance(
                field_value, str
            ), f"{field_value} is not an accepted value for ICAT (only strings are allowed)!"

        # Add other info keys (not metadata)
        infodict["__metadata_gathered__"] = True

        # Update the node's info
        self._node.info.update(infodict)

    @property
    def metadata_gathering_done(self):
        return self._node.info.get("__metadata_gathered__", False)

    def add_technique(self, technique):
        if self.is_closed:
            raise RuntimeError("The dataset is already closed")
        if isinstance(technique, FieldGroup):
            technique = technique.name
        available = self.definitions.techniques._fields
        if technique not in available:
            raise ValueError(f"Unknown technique ({available})")
        self._node.add_technique(technique)

    @property
    def techniques(self):
        """list of techniques used in this dataset"""
        tdict = self.definitions.techniques._asdict()
        return [tdict[name] for name in self._technique_names]

    @property
    def _technique_names(self):
        """list of technique names used in this dataset"""
        return self._node.techniques

    def finalize_metadata(self):
        # check if a definiton is provided otherwhise use
        # names of application definition
        if (
            not self.has_metadata_field("definition")
            and len(self._technique_names) != 0
        ):
            self.write_metadata_field("definition", " ".join(self._technique_names))

        self.write_metadata_field("endDate", datetime.datetime.now().isoformat())

    def close(self, icat_client, raise_on_error=True):
        """Close the dataset in Redis and send to ICAT.
        The dataset will not be closed when it has no data on disk.
        """
        if self.is_registered:
            msg = f"dataset {self.name} is already registered with ICAT"
            if raise_on_error:
                raise RuntimeError(msg)
            else:
                self._log_debug(msg)
                return
        if self.is_closed:
            msg = f"dataset {self.name} is already closed"
            if raise_on_error:
                raise RuntimeError(msg)
            else:
                self._log_debug(msg)
                return
        if not self.has_data:
            self._log_debug(f"dataset {self.name} is not closed because no data")
            return
        self._raw_close()
        self.register_with_icat(icat_client, raise_on_error=raise_on_error)
        if icat_client.expire_datasets_on_close:
            self.set_expiration_time(include_parents=False)

    def _raw_close(self):
        self.finalize_metadata()
        self.freeze_inherited_icat_metadata()
        self._node.info["__closed__"] = True
        self._log_debug(f"dataset {self.name} is closed")

    def confirm_registration(self, raise_on_error=True):
        """Called when it has been confirmed that the dataset was
        registered in ICAT.
        """
        if not self.is_closed:
            msg = f"dataset {self.name} is not closed"
            if raise_on_error:
                raise RuntimeError(msg)
            else:
                self._log_debug(msg)
                return
        if not self._node.info["__registered__"]:
            self.set_expiration_time(include_parents=False)
            self._node.info["__registered__"] = True
            self._log_debug("confirm dataset registration")

    def register_with_icat(self, icat_client, raise_on_error=True):
        """Only registered with ICAT when the path exists. This call is
        asynchronous which means we won't have confirmation of success
        or failure.
        """
        if self.is_registered:
            msg = f"dataset {self.name} is already registered with ICAT"
            if raise_on_error:
                raise RuntimeError(msg)
            else:
                self._log_debug(msg)
                return
        if not self.is_closed:
            msg = f"dataset {self.name} is not closed"
            if raise_on_error:
                raise RuntimeError(msg)
            else:
                self._log_debug(msg)
                return
        if not self.has_data:
            self._log_debug(
                f"dataset {self.name} is not registered because it has no data"
            )
            return
        self._raw_register_with_icat(icat_client)

    def _raw_register_with_icat(self, icat_client):
        """This call is asynchronous which means we won't have confirmation
        of success or failure.
        """
        self._log_debug(f"register dataset {self.name} with ICAT")
        collection = self.collection
        icat_client.store_dataset(
            proposal=collection.proposal.name,
            collection=collection.name,
            dataset=self.name,
            path=self.path,
            metadata=self.get_current_icat_metadata(),
        )

    @autocomplete_property
    def collection(self):
        if self._collection is None:
            if self._node.parent is not None:
                self._collection = DatasetCollection(self._node.parent)
        return self._collection

    @autocomplete_property
    def proposal(self):
        if self.collection is None:
            return None
        else:
            return self.collection.proposal

    @property
    def parent(self):
        return self.collection

    @property
    def scan_nodes(self):
        yield from self._node.children()

    @property
    def has_scans(self):
        try:
            next(self.scan_nodes)
        except StopIteration:
            return False
        else:
            return True

    @property
    def has_data(self):
        return os.path.exists(self.path)

    @property
    def is_closed(self):
        return self._node.is_closed

    @property
    def is_registered(self):
        return self._node.is_registered

    @autocomplete_property
    def description(self):
        # TODO: use Dataset_description when it gets introduced
        return self.get_metadata_field("Sample_description")

    @description.setter
    def description(self, value):
        # TODO: use Dataset_description when it gets introduced
        if value is not None:
            # TODO: remove this block when Dataset_description gets introduced
            sample_description = self.sample_description
            if sample_description:
                value = f"{sample_description} ({value})"
        self["Sample_description"] = value

    @autocomplete_property
    def sample_description(self):
        # TODO: use Dataset_description when it gets introduced
        return self.collection.sample_description

    @sample_description.setter
    def sample_description(self, value):
        # TODO: use Dataset_description when it gets introduced
        self.collection.sample_description = value

    @autocomplete_property
    def sample_name(self):
        return self.get_metadata_field("Sample_name")

    @sample_name.setter
    def sample_name(self, value):
        self["Sample_name"] = value

    @property
    def _data_db_names_depth(self) -> int:
        """The Redis node depth at which the data nodes exist
        """
        return 1
