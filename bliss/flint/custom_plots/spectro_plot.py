# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from __future__ import annotations

import logging
import numpy

from silx.gui import qt
from silx.gui import icons
from silx.gui.plot import Plot1D
from silx.gui.plot.items import axis as axis_mdl

from silx.gui.plot.matplotlib import FigureCanvasQTAgg

from matplotlib.figure import Figure
from mpl_toolkits.mplot3d import Axes3D

_logger = logging.getLogger(__name__)


class SpectroPlot(qt.QWidget):
    """
    """

    def __init__(self, parent=None):
        super(SpectroPlot, self).__init__(parent=parent)
        self.__data = {}
        self._box_min_max = None

        fig = Figure()
        self._ax = fig.add_subplot(111, projection="3d")
        self._ax.grid(False)
        self._ax.view_init(elev=21., azim=-85)
        self._ax.set_box_aspect(aspect=(1, 1, 1))

        self.__plot = FigureCanvasQTAgg(fig)
        self.__plot.setParent(self)

        layout = qt.QVBoxLayout(self)
        layout.addWidget(self.__plot)

    def setGraphTitle(self, title: str):
        self.__plot.setGraphTitle(title)

    def getPlotWidget(self):
        return self.__plot

    def clear(self):
        self.__data = {}
        self._ax.clear()

    def setData(self, **kwargs):
        self.__data = dict(kwargs)
        self.__safeUpdatePlot()

    def setBoxMinMax(self, mini, maxi):
        self._box_min_max = (mini, maxi)

    def __safeUpdatePlot(self):
        try:
            self.__updatePlot()
        except Exception:
            _logger.critical("Error while updating the plot", exc_info=True)

    def __updatePlot(self):
        self._ax.cla()
        self._ax.grid(False)
        self._ax.set_xlabel("X")
        self._ax.set_ylabel("Y")
        self._ax.set_zlabel("Z")
        if self._box_min_max:
            mini, maxi = self._box_min_max
            self._ax.set_xlim([mini[0], maxi[0]])
            self._ax.set_ylim([mini[1], maxi[1]])
            self._ax.set_zlim([mini[2], maxi[2]])

        # self._ax.set_xlim([-10, 10])
        # self._ax.set_ylim([-10, 10])
        # self._ax.set_zlim([-10, 10])

        for args, kwargs in self.__data["quivers"]:
            self._ax.quiver(*args, **kwargs)

        for args, kwargs in self.__data["plots"]:
            self._ax.plot(*args, **kwargs)

        for args, kwargs in self.__data["scatters"]:
            self._ax.scatter(*args, **kwargs)

        for args, kwargs in self.__data["surfaces"]:
            # self._ax.plot_surface(*args, **kwargs)
            self._ax.plot_trisurf(*args, **kwargs)

        self.__plot.draw()
