# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from operator import pos
import time
import numpy
import weakref

from bliss.common import event
from bliss.common.axis import Axis, AxisState
from bliss.common.soft_axis import SoftAxis
from bliss.common.protocols import counter_namespace
from bliss.common.utils import autocomplete_property
from bliss.common.plot import get_flint

from bliss.physics.units import ur
from bliss.physics.diffraction import CrystalPlane, Crystal, BasePlane
from bliss.controllers.motor import CalcController
from bliss.controllers.bliss_controller import BlissController
from bliss.config.settings import HashObjSetting


"""  Spectrometer geometry convention: 

    Points:
      S: point of interaction between the sample and the beam (origin of spectrometer referential)
      A0: center of the central analyser  (virtual, A0_y = A0_z = 0)
      D0: center of the detector (D0_y = 0)
      Ai: center of analyser i
      Di: position on detector of the beam reflected by the analyser Ai
    
    Vectors:
      SA: Sample to Analyser
      SD: Sample to Detector
      NA: normal to analyser surface on point A (optical)
      NAc: normal to analyser crystal plane on point A (diffraction)
    
    Planes:
      Meridional plane (MeP): defined by the 3 points < S, Ai, Di >
      Scattering plane (ScP): contains SA and normal to MeP
      Sagittal   plane (SgP): contains NA and normal to MeP
    
    Spectrometer referentials:
      Origin: centred on S
      +X: SA0 / norm(SA0) (perpendicular to incoming beam and contained in the equatorial plane)
      +Y: colinear to incoming beam
      +Z: X ^ Y (pointing toward sky)

"""

"""

    # YML Configuration

    spectrometers:

        - name: spectro
            plugin: generic
            module: spectrometers.spectrometer_base
            class: Spectrometer
            surface_radius_meridional: 5
            surface_radius_sagittal: 10
            crystal: Si111
            miscut: 0

            analysers:
            - name: a0
                angular_offset: 0 
                offset_on_detector: 0
                rpos: $xa0
                zpos: $za0
                pitch: $pita0
                yaw: $yawa0

            - name: a1
                angular_offset: 14 
                offset_on_detector: 0
                rpos: $xa1
                zpos: $za1
                pitch: $pita1
                yaw: $yawa1

            - name: a2
                # angular_offset: -14
                offset_on_detector: 0
                ypos: 2
                xpos: $xa2
                # rpos: $xa2
                zpos: $za2
                pitch: $pita2
                yaw: $yawa2
                

            detector: 
            - name: det0
                xpos: $xdet
                ypos: 0
                zpos: $zdet
                pitch: $pitdet
                yaw: 0

"""


# --------------- Note about array and matrix algebra ---------------------
#
# with a vector:
# V = | 1
#     | 0
#     | 0
# expressed as: V = numpy.array( [1, 0, 0] )
#
# with a matrix:
# P = |  0  1  0 |
#     | -1  0  0 |
#     |  0  0  1 |
# expressed as: P = numpy. array( [ [0, 1, 0], [ -1, 0, 0], [0, 0, 1] ] )
#
#
# then VV = P@V  is the matrix product:
#
# | 0 = |  0  1  0 | * | 1
# |-1   | -1  0  0 |   | 0
# | 0   |  0  0  1 |   | 0
#
# with VV = numpy.array( [0, -1, 0] )
#
# --------------------------------------------------------------------------


def rotation_matrix(axis, angle):
    """
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by a given angle (degree).
    """
    theta = numpy.deg2rad(angle)
    axis = axis / getnorm(axis)
    a = numpy.cos(theta / 2.0)
    b, c, d = -axis * numpy.sin(theta / 2.0)
    aa, bb, cc, dd = a * a, b * b, c * c, d * d
    bc, ad, ac, ab, bd, cd = b * c, a * d, a * c, a * b, b * d, c * d
    return numpy.array(
        [
            [aa + bb - cc - dd, 2 * (bc + ad), 2 * (bd - ac)],
            [2 * (bc - ad), aa + cc - bb - dd, 2 * (cd + ab)],
            [2 * (bd + ac), 2 * (cd - ab), aa + dd - bb - cc],
        ]
    )


def getnorm(vector):
    return numpy.sqrt(numpy.dot(vector, vector))


def normalize(vector):
    n = getnorm(vector)
    if n == 0:
        return numpy.array([0, 0, 0])
    else:
        return vector / n


def get_normal_plane(vector, w, h, center):
    w, h = w / 2, h / 2
    ex = normalize(vector)
    ez = numpy.array([0, 0, 1])
    ey = numpy.cross(ez, ex)
    ez = numpy.cross(ex, ey)

    p0 = ez * h - ey * w
    p1 = ez * h + ey * w
    p2 = -p0
    p3 = -p1

    return [p0 + center, p1 + center, p2 + center, p3 + center]


def get_angle(v1, v2):
    n1 = getnorm(v1)
    n2 = getnorm(v2)
    return numpy.rad2deg(numpy.arccos(numpy.dot(v1, v2) / (n1 * n2)))


class VirtualAxis:
    def __init__(self, name, pos):
        self.name = name
        self._position = pos

    def position(self):
        return self._position

    def move(self, pos):
        if not numpy.isclose(
            pos, self._position, rtol=1e-03
        ):  # TO DO: SHOULD BE MANAGED DIFFERENTLY BY NOT MOVING FIXED AXIS
            print(f"Warning: {self.name} has been modified: {self._position} => {pos}")
            self._position = pos

    def stop(self):
        pass

    def state(self):
        return AxisState("READY")


class Positioner3D:
    """" Manages position (x, y, z) and orientation (pitch, yaw) of an object in 3D """

    def __init__(self, cfg):
        self._config = cfg
        self._name = cfg["name"]

    def __info__(self):
        txt = f" axes:\n"
        for tag in ["xpos", "ypos", "zpos", "pitch", "yaw"]:
            txt += f"  - {tag:10s}: {str(self._get_pos(tag)):10s}\n"

        return txt

    def _direction_to_angles(self, direction):
        x, y, z = direction
        r = getnorm(direction)
        if r != 0:
            pitch = -numpy.rad2deg(numpy.arcsin(z / r))
            yaw = numpy.rad2deg(numpy.arctan2(y, x))
        else:
            pitch = yaw = 0

        return pitch, yaw

    def _get_pos(self, tag):
        raise NotImplementedError

    def _set_pos(self, tag, value):
        raise NotImplementedError

    @property
    def name(self):
        return self._name

    @property
    def config(self):
        return self._config

    @property
    def xpos(self):
        return self._get_pos("xpos")

    @xpos.setter
    def xpos(self, value):
        self._set_pos("xpos", value)

    @property
    def ypos(self):
        return self._get_pos("ypos")

    @ypos.setter
    def ypos(self, value):
        self._set_pos("ypos", value)

    @property
    def zpos(self):
        return self._get_pos("zpos")

    @zpos.setter
    def zpos(self, value):
        self._set_pos("zpos", value)

    @property
    def pitch(self):
        return self._get_pos("pitch")

    @pitch.setter
    def pitch(self, value):
        self._set_pos("pitch", value)

    @property
    def yaw(self):
        return self._get_pos("yaw")

    @yaw.setter
    def yaw(self, value):
        self._set_pos("yaw", value)

    @property
    def position(self):
        """ Return the position (x, y, z) as a numpy array """
        return numpy.array([self.xpos, self.ypos, self.zpos])

    @property
    def direction(self):
        """ Compute the direction vector based on the current pitch and yaw angles (normalized).
            If pitch = yaw = 0 the direction vector is colinear to the x axis of this object referential.
        """

        # start with a default direction along ex and apply pitch and yaw rotations
        v = numpy.array([1, 0, 0])

        pit = numpy.deg2rad(self.pitch)
        yaw = numpy.deg2rad(self.yaw)

        cp = numpy.cos(pit)
        sp = numpy.sin(pit)
        cy = numpy.cos(yaw)
        sy = numpy.sin(yaw)

        # Rx = numpy. array([[1, 0, 0], [ 0, cr, -sr], [0, sr, cr]]) # rot mat around ex (roll)
        Ry = numpy.array(
            [[cp, 0, sp], [0, 1, 0], [-sp, 0, cp]]
        )  # rot mat around ey (pitch)
        Rz = numpy.array(
            [[cy, -sy, 0], [sy, cy, 0], [0, 0, 1]]
        )  # rot mat around ez (yaw)

        v = Rz @ Ry @ v  # Rz @ Ry @ Rx @ v  # apply rotations in the order:  Z->Y->X

        # ============ other implementation ===============
        # pit = -pit
        # z = numpy.sin(pit)
        # x = numpy.sqrt((1 - numpy.sin(pit) ** 2) / (1 + numpy.tan(yaw) ** 2))
        # y = x * numpy.tan(yaw)

        # # print("v", v)
        # # print("x,y,z",x,y,z)
        # # print(numpy.isclose(v, numpy.array([x,y,z]), rtol=1e-5 ) )

        # assert all(numpy.isclose(v, numpy.array([x, y, z]), rtol=1e-5))

        return v


class Analyser(Positioner3D):
    def __init__(self, cfg, spectro):
        super().__init__(cfg)
        self._spectro = spectro
        self._load_config()

    def _load_config(self):

        self._bragg_solution = None
        self._offset_on_detector = self.config["offset_on_detector"]

        axes_tags = []

        # check case (r, alpha) or (x, yoff)
        if all(i in self.config.keys() for i in ["rpos", "xpos"]):
            txt = "Spectro config error: cannot use 'rpos' and 'xpos' at the same time, select one.\n"
            txt += "\nCylindrical case:\n"
            txt += "  - 'rpos': analyser radial axis (distance to center) in the (X,Y) plan\n"
            txt += "  - 'angular_offset': analyser angular offset toward X in the (X,Y) plan\n"
            txt += "\nCartesian case:\n"
            txt += "  - 'xpos': analyser X axis (x coordinate in spectrometer referential)\n"
            txt += "  - 'ypos': analyser Y axis or position (y coordinate in spectrometer referential)\n"
            raise ValueError(txt)

        if "rpos" in self.config.keys():
            self._referential_mode = "cylindrical"
            self._angular_offset = self.config["angular_offset"]
            self._ypos = None
            axes_tags = ["rpos", "zpos", "pitch", "yaw"]

        elif "xpos" in self.config.keys():
            self._referential_mode = "cartesian"
            self._angular_offset = None
            self._ypos = self.config["ypos"]
            axes_tags = ["xpos", "ypos", "zpos", "pitch", "yaw"]

        self._axes = {}
        for tag in axes_tags:
            axis = self.config[tag]

            if not isinstance(axis, Axis):
                name = f"{self.name}_{tag}"
                pos = float(axis)
                vaxis = VirtualAxis(name, pos)
                axis = SoftAxis(
                    name,
                    vaxis,
                    position="position",
                    move="move",
                    stop="stop",
                    state="state",
                    low_limit=float("-inf"),
                    high_limit=float("+inf"),
                    tolerance=1e-4,
                    export_to_session=False,
                )

            self._axes[tag] = axis

    def __info__(self):
        txt = f"--- Analyser: {self.name} ---\n"
        txt += f" angular_offset   : {self.angular_offset}\n"
        txt += f" offset_on_detector : {self.offset_on_detector}\n"
        txt += super().__info__()
        return txt

    def _analyser_to_spectro_matrix(self, position):
        """ compute the matrix to switch from Anlayser to Spectro
            by expressing analyser base into spectrometer base 
        """
        incident = normalize(position) * -1
        ex = incident  # x axis of analyser base is along incident beam (i.e AS)
        ez = numpy.array([0, 0, 1])  # analyser yaw around lab-Z
        ey = numpy.cross(ez, ex)
        ez = numpy.cross(ex, ey)  # analyser yaw around ex^ey = local ez
        p = numpy.vstack((ex, ey, ez)).T  # analyser to spectro matrix
        return p

    def _get_pos(self, tag):
        if self._referential_mode == "cylindrical":
            if tag == "xpos":
                ang = -numpy.deg2rad(self.angular_offset)
                return self._axes["rpos"].position * numpy.cos(ang)
            elif tag == "ypos":
                ang = -numpy.deg2rad(self.angular_offset)
                return self._axes["rpos"].position * numpy.sin(ang)

        if self._referential_mode == "cartesian":
            if tag == "rpos":
                x = self._axes["xpos"].position
                y = self._axes["ypos"].position
                return numpy.sqrt(x ** 2 + y ** 2)

        return self._axes[tag].position

    def _set_pos(self, tag, value):
        if self._referential_mode == "cylindrical":
            if tag in ["xpos", "ypos"]:
                print(f"cannot set {tag} in cylindrical mode")
                return

        if self._referential_mode == "cartesian":
            if tag == "rpos":
                print(f"cannot set {tag} in cartesian mode")
                return

        self._axes[tag].move(value)

    @property
    def axes(self):
        return self._axes

    @property
    def angular_offset(self):
        return self._angular_offset

    @angular_offset.setter
    def angular_offset(self, value):
        self._angular_offset = value
        self._spectro._update()

    @property
    def offset_on_detector(self):
        return self._offset_on_detector

    @offset_on_detector.setter
    def offset_on_detector(self, value):
        self._offset_on_detector = value
        self._spectro._update()

    @property
    def incident(self):
        """ Compute the normalized direction of the incident ray (from analyser to sample !)
        """
        return normalize(self.position) * -1

    @property
    def normal(self):
        """ Compute the normal vector of the analyser (normalized).
            The normal is the vector perpendicular to the analyser surface at its center and
            expressed in the spectrometer referential. 
        """
        p = self._analyser_to_spectro_matrix(self.position)
        return p @ self.direction

    # @normal.setter
    # def normal(self, vect):
    #     # p = numpy.transpose(self._analyser_to_spectro_matrix(self.position))
    #     # vect = p @ vect # vect in analyser ref

    #     x, y, z = vect
    #     ex = self.incident
    #     vxy = numpy.array([x, y, 0])
    #     nvxy = getnorm(vxy)
    #     vxz = numpy.array([x, 0, z])
    #     nvxz = getnorm(vxz)
    #     self.yaw = -numpy.rad2deg(numpy.arccos(numpy.dot(vxy, ex) / nvxy))
    #     self.pitch = -numpy.rad2deg(numpy.arccos(numpy.dot(vxz, ex) / nvxz))

    @property
    def reflected(self):
        """ Compute the direction of the reflected ray toward surface normal (normalized).
        """
        # reflection is obtained by rotating the incident ray by 180 degree around the analyser surface normal
        return numpy.dot(rotation_matrix(self.normal, 180), self.incident)

    @property
    def meridional(self):
        """ Compute the normal to the meridional plan (normalized).
        """
        return normalize(numpy.cross(self.normal, self.incident))

    @property
    def sagittal(self):
        """ Compute the normal to the sagittal plan (normalized).
        """
        return normalize(
            numpy.cross(self.normal, numpy.array([0, 1, 0]))
        )  # self.meridional))


class Detector(Positioner3D):
    def __init__(self, cfg, spectro):
        super().__init__(cfg)
        self._spectro = spectro
        self._load_config()

    def _load_config(self):
        self._axes = {}
        for tag in ["xpos", "ypos", "zpos", "pitch", "yaw"]:
            axis = self.config[tag]
            if not isinstance(axis, Axis):
                name = f"{self.name}_{tag}"
                pos = float(self.config[tag])
                vaxis = VirtualAxis(name, pos)
                axis = SoftAxis(
                    name,
                    vaxis,
                    position="position",
                    move="move",
                    stop="stop",
                    state="state",
                    low_limit=float("-inf"),
                    high_limit=float("+inf"),
                    tolerance=1e-4,
                    export_to_session=False,
                )

            self._axes[tag] = axis

    def __info__(self):
        txt = f"\n--- Detector: {self.name} ---\n"
        txt += super().__info__()
        return txt

    def _get_pos(self, tag):
        return self._axes[tag].position

    def _set_pos(self, tag, value):
        self._axes[tag].move(value)

    @property
    def axes(self):
        return self._axes


class Spectrometer(BlissController):
    def __init__(self, config):
        super().__init__(config)

        # self._settings = HashObjSetting(f"{self.name}_settings") # is name enough ?
        self._current_bragg_solution = None  # self._settings.get('bragg_solution')

        self._detector_target = None
        self._detector_mode = "A"

        self._plot = None
        self._plot_axes_connected = False

    def __info__(self):
        txt = f"=== Spectrometer: {self.name} ===\n\n"

        txt += f" crystal: {self.crystal}\n"
        txt += f" miscut: {self.miscut}\n"
        txt += f" meridional curvature: {self.surface_radius_meridional}\n"
        txt += f" sagittal curvature: {self.surface_radius_sagittal}\n"
        txt += "\n"

        for ana in self.analysers:
            txt += ana.__info__()
            txt += "\n"

        txt += self.detector.__info__()

        return txt

    def _get_subitem_default_class_name(self, cfg, parent_key):
        # Called when the class key cannot be found in the item_config.
        # Then a default class must be returned. The choice of the item_class is usually made from the parent_key value.
        # Elements of the item_config may also by used to make the choice of the item_class.

        """ 
            Return the appropriate default class name (as a string) for a given item.
            args: 
                - cfg: item config node
                - parent_key: the key under which item config was found
        """

        if parent_key == "analysers":
            return "Analyser"

        elif parent_key == "detector":
            return "Detector"

        else:
            raise NotImplementedError

    def _create_subitem_from_config(
        self, name, cfg, parent_key, item_class, item_obj=None
    ):
        # Called when a new subitem is created (i.e accessed for the first time via self._get_subitem)
        """ 
            Return the instance of a new item owned by this container.

            args:
                name: item name
                cfg : item config
                parent_key: the config key under which the item was found (ex: 'counters').
                item_class: a class to instantiate the item (None if item is a reference)
                item_obj: the item instance (None if item is NOT a reference)

            return: item instance
                
        """

        if item_obj is not None:
            return item_obj

        if parent_key == "analysers":
            return item_class(cfg, self)

        elif parent_key == "detector":
            return item_class(cfg, self)

        else:
            raise NotImplementedError

    def _load_config(self):
        # Called by the plugin via self._initialize_config
        # Called after self._subitems_config has_been filled.

        """
            Read and apply the YML configuration of this container. 
        """

        self._miscut = self.config["miscut"]
        self._surface_radius_meridional = self.config["surface_radius_meridional"]
        self._surface_radius_sagittal = self.config["surface_radius_sagittal"]

        if self.config.get("dspacing"):
            self._crystal = BasePlane(self.config.get("dspacing"))
        else:
            self._crystal = CrystalPlane.fromstring(self.config["crystal"])

        self._analysers = []
        for name, (cfg, pkey) in self._subitems_config.items():
            if pkey == "analysers":
                self._analysers.append(self._get_subitem(name))
            elif pkey == "detector":
                self._detector = self._get_subitem(name)

        # prepare calc mot config
        axes_cfg = []
        for ana in self._analysers:
            for tag, axis in ana.axes.items():
                axes_cfg.append({"name": axis, "tags": f"real {ana.name}_{tag}"})

        for tag, axis in self.detector.axes.items():
            axes_cfg.append({"name": axis, "tags": f"real {self.detector.name}_{tag}"})

        axes_cfg.append({"name": f"{self.name}_bragg", "tags": "bragg"})

        self._calc_mot = SpectroBraggCalcController(self, {"axes": axes_cfg})
        self._calc_mot._initialize_config()

        axes_cfg = []
        bragg_axis = self._calc_mot._tagged["bragg"][0]
        axes_cfg.append({"name": bragg_axis, "tags": f"real bragg"})
        axes_cfg.append({"name": f"{self.name}_energy", "tags": "energy"})
        self._ene_calc = SpectroEnergyCalcController(self, {"axes": axes_cfg})
        self._ene_calc._initialize_config()

    def _init(self):
        # Called by the plugin via self._initialize_config
        # Called just after self._load_config

        """
            Place holder for any action to perform after the configuration has been loaded.
        """
        self._update()

    def _get_axis(self, analyser, tag):
        # tag = f"{ana.name}_{tag}"
        # return self.calc_mot._tagged[tag][0]
        return analyser.axes[tag]

    def _get_plot_data(self):

        colors = [
            [0, 212, 255],
            [0, 17, 255],
            [204, 0, 255],
            [255, 0, 42],
            [75, 0, 130],
            [238, 130, 238],
            [123, 104, 238],
            [153, 50, 204],
            [255, 127, 80],
            [224, 255, 255],
            [135, 206, 250],
            [70, 130, 180],
            [65, 105, 225],
            [30, 144, 255],
            [0, 97, 255],
            [93, 0, 255],
            [255, 0, 61],
        ]

        # === quivers ==============================
        quivers = []
        plots = []
        scatters = []
        surfaces = []

        # spectrometer base vectors
        vectors = []
        vectors.append([0, 0, 0, 1, 0, 0])  # ex
        vectors.append([0, 0, 0, 0, 1, 0])  # ey
        vectors.append([0, 0, 0, 0, 0, 1])  # ez
        quivers.append((zip(*numpy.array(vectors)), {"color": "darkgray"}))

        # beam vector ==============================
        vectors = []
        vectors.append([0, -1, 0, 0, 1, 0])
        quivers.append((zip(*numpy.array(vectors)), {"color": "red"}))

        # detector
        # [D0, N0d, pitch, yaw] = self._current_bragg_solution[1][self.detector]
        scatters.append((zip(self.detector.position), {"color": "lightgreen"}))
        quivers.append(
            (
                zip(numpy.hstack((self.detector.position, self.detector.direction))),
                {"color": "lightgreen"},
            )
        )

        pts = get_normal_plane(self.detector.direction, 2, 2, self.detector.position)
        surfaces.append(
            (
                zip(*numpy.vstack(pts)),
                {
                    "color": "lightgreen",
                    "linewidth": 0,
                    "antialiased": False,
                    "alpha": 0.3,
                },
            )
        )

        # add planes
        # l = 10
        # z0 = 0
        # X = numpy.array([-l, l])
        # Y = numpy.array([-l, l])
        # X, Y = numpy.meshgrid(X, Y)
        # Z = X * z0
        # pscatt = ax.plot_surface(
        #     X,
        #     Y,
        #     Z,
        #     color="lightblue",  # cmap=cm.coolwarm,
        #     linewidth=0,
        #     antialiased=False,
        #     alpha=0.3,
        # )

        #
        for id, ana in enumerate(self._analysers):

            # --- current positionning and ray tracing ----------------------
            col = list(numpy.array(colors[id % len(colors)]) / 255.)
            pos = ana.position
            meri = ana.meridional
            sagi = ana.sagittal
            mradius = ana.normal * self.surface_radius_meridional
            sradius = ana.normal * self.surface_radius_sagittal
            mcenter = pos + mradius
            scenter = pos + sradius
            dist = getnorm(pos)
            refl = ana.reflected * dist * 1.2

            # ray tracing: sample => analyser => reflection
            pts = [[0, 0, 0], list(pos), list(pos + refl)]
            plots.append(
                (
                    zip(*numpy.array(pts)),
                    {
                        "color": "red",
                        "linestyle": "solid",
                        "linewidth": 0.8,
                        "alpha": 0.3,
                    },
                )
            )

            # meridional plan center and normal
            quivers.append(
                (zip(*numpy.array([list(mcenter) + list(meri)])), {"color": col})
            )

            # rowland and sagittal circles
            rowland_circles = []
            sagittal_circles = []
            for theta in range(361):
                rowland_circles.append(
                    list(numpy.dot(rotation_matrix(meri, theta), mradius) + mcenter)
                )
                sagittal_circles.append(
                    list(numpy.dot(rotation_matrix(sagi, theta), sradius) + scenter)
                )
            plots.append(
                (
                    zip(*numpy.array(rowland_circles)),
                    {"color": col, "linestyle": "dotted", "linewidth": 0.8},
                )
            )
            plots.append(
                (
                    zip(*numpy.array(sagittal_circles)),
                    {"color": col, "linestyle": "dotted", "linewidth": 0.8},
                )
            )

            # meridional circle radius
            pts = [list(pos), list(mcenter)]
            plots.append(
                (
                    zip(*numpy.array(pts)),
                    {"color": col, "linestyle": "dotted", "linewidth": 0.8},
                )
            )

            # sagittal circle radius
            pts = [list(pos), list(scenter)]
            plots.append(
                (
                    zip(*numpy.array(pts)),
                    {"color": col, "linestyle": "dotted", "linewidth": 0.8},
                )
            )

            # current bragg solution
            if self._current_bragg_solution:
                [Ai, Di, Ri, Nia, Nid, pitch, yaw] = self._current_bragg_solution[1][
                    ana
                ]

                # Detector position and Rowland center
                scatters.append((zip(*numpy.array([Di, Ri])), {"color": col}))

                # Detector direction
                # x, y, z = Di
                # u, v, w = Nid
                # quivers.append(((x, y, z, u, v, w), {"color": col}))

                # Analyser direction
                x, y, z = Ai
                u, v, w = Nia
                quivers.append(((x, y, z, u, v, w), {"color": col}))

                # samlpe to detecor
                pts = [[0, 0, 0], list(Di)]
                plots.append(
                    (
                        zip(*numpy.array(pts)),
                        {"color": col, "linestyle": "dotted", "linewidth": 0.8},
                    )
                )

        return {
            "quivers": quivers,
            "plots": plots,
            "scatters": scatters,
            "surfaces": surfaces,
        }

    def _compute_central_analyser_solution(self, bragg):

        """ Compute the position of the virtual central analyser and the corresponding detector and rowland circle centers positions.
            It assumes that the central analyser is always along the X axis of the spectrometer referential (i.e y0=0 and z0=0).
            args:
                - bragg: bragg angle in degree
            return:
                [A0, D0, R0]: a list of 3 vectors for the positions of [Analyser, Detector, Rowland]
        """

        bragg = numpy.deg2rad(bragg)
        miscut = numpy.deg2rad(self.miscut)
        angm = bragg - miscut
        angp = bragg + miscut

        rad = 2 * self.surface_radius_meridional  # Rowland circle

        A0x = rad * numpy.sin(angm)
        A0y = 0
        A0z = 0

        D0x = rad * numpy.cos(angp) * numpy.sin(2 * bragg)
        D0y = 0
        D0z = rad * numpy.sin(angp) * numpy.sin(2 * bragg)

        R0x = rad * numpy.sin(angm) / 2
        R0y = 0
        R0z = rad * numpy.cos(angm) / 2

        A0 = numpy.array([A0x, A0y, A0z])
        D0 = numpy.array([D0x, D0y, D0z])
        R0 = numpy.array([R0x, R0y, R0z])

        return [A0, D0, R0]

    def compute_bragg_solution(self, bragg_angle):
        """ Compute for all analysers the positions of:
                - Ai: analyser center
                - Di: associated beam position on detector
                - Ri: associated rowland circle center
                - Nia: the normal to analyser surface
                - Nid: the normal to detector surface (pointing toward Rowland center)
                - pitch: the analyser pitch angle (degree)
                - yaw: the analyser yaw angle (degree)
            args:
                - bragg: bragg angle in degree
            return:
                { anaylser: [Ai, Di, Ri, Nia, Nid, pitch, yaw], }
        """

        # compute positioning of the virtual central analyser A0
        # wich is always along the X axis of the spectrometer referential (i.e y0=0 and z0=0)
        A0, D0, R0 = self._compute_central_analyser_solution(bragg_angle)
        X0 = numpy.array([1, 0, 0])

        D0norm = getnorm(D0)
        D0z = D0[2]
        D0x = D0[0]
        D0x2 = D0[0] ** 2
        N0a = D0 - 2 * A0
        N0a = N0a / getnorm(N0a)
        N0d = R0 - D0
        N0d = N0d / getnorm(N0d)

        solution = {}
        reals_pos = {}

        # then compute solution for all analysers
        for ana in self.analysers:

            if ana.angular_offset is None:
                beta = -numpy.arcsin(ana.ypos / A0[0])
            else:
                beta = numpy.deg2rad(ana.angular_offset)

            psi = numpy.arcsin(-ana.offset_on_detector / D0z)

            Di = numpy.dot(rotation_matrix(X0, numpy.rad2deg(psi)), D0)

            # use a.cosx + b.sinx = c =>   sinx(x+p) = c/sqrt(a**2+b**2) and sin(p) = a/sqrt(a**2+b**2)
            a = D0z * (D0z * numpy.tan(beta) - D0x * numpy.sin(psi))
            b = -D0z * D0norm * numpy.cos(psi)
            c = -D0x2 * numpy.tan(beta) - D0x * D0z * numpy.sin(psi)

            nab = numpy.sqrt(a * a + b * b)
            d = numpy.arcsin(c / nab)
            e = numpy.arcsin(a / nab)
            eta = d - e

            Ai = numpy.dot(rotation_matrix(Di, numpy.rad2deg(eta)), A0)

            Ri = numpy.dot(rotation_matrix(X0, numpy.rad2deg(psi)), R0)
            Ri = numpy.dot(rotation_matrix(Di, numpy.rad2deg(eta)), Ri)

            Nia = numpy.dot(rotation_matrix(X0, numpy.rad2deg(psi)), N0a)
            Nia = numpy.dot(rotation_matrix(Di, numpy.rad2deg(eta)), Nia)

            Nid = numpy.dot(rotation_matrix(X0, numpy.rad2deg(psi)), N0d)
            Nid = numpy.dot(rotation_matrix(Di, numpy.rad2deg(eta)), Nid)

            p = numpy.linalg.inv(
                ana._analyser_to_spectro_matrix(Ai)
            )  # spectro2analyser
            d = p @ Nia
            pitch, yaw = ana._direction_to_angles(d)
            solution[ana] = [Ai, Di, Ri, Nia, Nid, pitch, yaw]

            dico = {
                "xpos": Ai[0],
                "ypos": Ai[1],
                "zpos": Ai[2],
                "rpos": numpy.sqrt(Ai[0] ** 2 + Ai[1] ** 2),
                "pitch": pitch,
                "yaw": yaw,
            }

            for tag in ana.axes.keys():
                reals_pos[f"{ana.name}_{tag}"] = dico[tag]

        # detector solution
        target = self.detector_target
        if self.detector_mode == "A":
            if isinstance(target, Analyser):
                Ai, Di = solution[target][0:2]
                vect = Ai - Di
            else:
                vect = A0 - D0

        elif self.detector_mode == "R":
            if isinstance(target, Analyser):
                Di, Ri = solution[target][1:3]
                vect = Ri - Di
            elif target is None:
                vect = N0d

        pitch, yaw = self.detector._direction_to_angles(vect)
        solution[self.detector] = [D0, N0d, pitch, yaw]

        [D0, N0d, pitch, yaw] = solution[self.detector]
        reals_pos[f"{self.detector.name}_xpos"] = D0[0]
        reals_pos[f"{self.detector.name}_ypos"] = D0[1]
        reals_pos[f"{self.detector.name}_zpos"] = D0[2]
        reals_pos[f"{self.detector.name}_pitch"] = pitch
        reals_pos[f"{self.detector.name}_yaw"] = yaw

        self._current_bragg_solution = (bragg_angle, solution, reals_pos)
        return reals_pos

    @autocomplete_property
    def calc_mot(self):
        return self._calc_mot

    @autocomplete_property
    def analysers(self):
        return counter_namespace(self._analysers)

    @autocomplete_property
    def detector(self):
        return self._detector

    @autocomplete_property
    def crystal(self):
        return self._crystal

    @crystal.setter
    def crystal(self, value):
        self._crystal = CrystalPlane.fromstring(value)
        self._update()

    @property
    def surface_radius_meridional(self):
        return self._surface_radius_meridional

    @surface_radius_meridional.setter
    def surface_radius_meridional(self, value):
        self._surface_radius_meridional = value
        self._update()

    @property
    def surface_radius_sagittal(self):
        return self._surface_radius_sagittal

    @surface_radius_sagittal.setter
    def surface_radius_sagittal(self, value):
        self._surface_radius_sagittal = value
        self._update()

    @property
    def miscut(self):
        return self._miscut

    @miscut.setter
    def miscut(self, value):
        self._miscut = value
        self._update()

    @property
    def bragg(self):
        return self.calc_mot._tagged["bragg"][0].position

    @bragg.setter
    def bragg(self, value):
        self.calc_mot._tagged["bragg"][0].move(value)
        self._update_plot(forced=True)

    @autocomplete_property
    def energy_axis(self):
        return self._ene_calc._tagged["energy"][0]

    @property
    def detector_target(self):
        return self._detector_target

    @detector_target.setter
    def detector_target(self, target):
        """ Select the target for the detector alignment.
            The detector surface normal will point to that target.
            It also depends on the 'detector_mode' which decides 
            if it points to the analyser center or associated rowland center.
            
            Possibe values for the target: 
              - None: point to the virtual central analyser (default)
              - analyser: one of the spectrometer analysers
        """

        if (target is not None) and (target not in self.analysers):
            raise ValueError("target must be None or one of the spectrometer analysers")

        self._detector_target = target

        self._update()

    @property
    def detector_mode(self):
        return self._detector_mode

    @detector_mode.setter
    def detector_mode(self, mode):
        """ Select the mode for the detector alignment.
            The detector surface normal will point to that target point.
            Possibe values for the mode: 
              - 'A': point to the center of an analyser (default) (see self.detector_target to select the analyser)
              - 'R': point to the Rowland center of an analyser   (see self.detector_target to select the analyser)
              

        """
        if mode not in ["A", "R"]:
            raise ValueError(f"mode must be 'A' or 'R' ")

        self._detector_mode = mode

        self._update()

    @property
    def plot(self):
        if self._plot is None:
            self._plot = SpectroPlot(self)
            self._connect_to_axes_moves()
            self._plot_finalizer = weakref.finalize(self._plot, self._disconnect_axes)

        if not self._plot.is_active():
            self._plot.create_plot()

        self._plot.update_plot(forced=True)

        return self._plot

    def _connect_to_axes_moves(self):
        if not self._plot_axes_connected:
            for ana in self.analysers:
                for ax in ana.axes.values():
                    # print(f"connecting {ax.name}")
                    event.connect(ax, "position", self._update_plot)
                    event.connect(ax, "move_done", self._update_plot)
            self._plot_axes_connected = True

    def _disconnect_axes(self):
        if self._plot_axes_connected:
            for ana in self.analysers:
                for ax in ana.axes.values():
                    event.disconnect(ax, "position", self._update_plot)
                    event.disconnect(ax, "move_done", self._update_plot)
                    # print(f"disconnecting {ax.name}")
            self._plot_axes_connected = False

    def _update_plot(self, position=None, sender=None, forced=False):
        if self._plot:
            self._plot.update_plot(forced)

    def _update(self):
        if self.bragg != numpy.nan:
            print("=== Update spectrometer", self.name)
            self.compute_bragg_solution(self.bragg)
            self._update_plot(forced=True)

    def check_solution(self):
        if not self._current_bragg_solution:
            print("No solution yet")
            return

        bragg, soluce, reals_pos = self._current_bragg_solution

        txt = f" --- Bragg solution @ {bragg} degree ---- \n\n"

        for ana in self.analysers:

            [Ai, Di, Ri, Nia, Nid, pitch, yaw] = soluce[ana]

            txt += f"   --- analyser: {ana.name} \n\n"

            txt += f"    cur_pitch_yaw = {ana.pitch:.4f} {ana.yaw:.4f}\n"
            txt += f"    sol_pitch_yaw = {pitch:.4f} {yaw:.4f}\n\n"

            txt += f"    cur_pos = {ana.position[0]:.4f} {ana.position[1]:.4f} {ana.position[2]:.4f}\n"
            txt += f"    sol_pos = {Ai[0]:.4f} {Ai[1]:.4f} {Ai[2]:.4f}\n\n"

            txt += f"    cur_dir = {ana.normal[0]:.4f} {ana.normal[1]:.4f} {ana.normal[2]:.4f}\n"
            txt += f"    sol_dir = {Nia[0]:.4f} {Nia[1]:.4f} {Nia[2]:.4f}\n\n"

            rpos = ana.position + ana.normal * self.surface_radius_meridional
            txt += f"    cur_row = {rpos[0]:.4f} {rpos[1]:.4f} {rpos[2]:.4f}\n"
            txt += f"    sol_row = {Ri[0]:.4f} {Ri[1]:.4f} {Ri[2]:.4f}\n\n"

            txt += (
                f"    cur_incident_angle = {get_angle(ana.incident, ana.normal):.4f} \n"
            )
            txt += f"    sol_incident_angle = {get_angle(ana.incident, Nia):.4f} \n\n"

            txt += f"    cur_reflected_angle = {get_angle(ana.reflected, ana.normal):.4f} \n"
            txt += f"    sol_reflected_angle = {get_angle(ana.reflected, Nia):.4f} \n\n"

            txt += "\n"

        print(txt)

    def energy2bragg(self, energy):
        return self.crystal.bragg_angle(energy * ur.keV).to(ur.deg).magnitude

    def bragg2energy(self, bragg):
        return self.crystal.bragg_energy(bragg * ur.deg).to(ur.keV).magnitude


class SpectroBraggCalcController(CalcController):
    def __init__(self, spectro, cfg):
        super().__init__(cfg)
        self.spectro = spectro
        self.tolerance = 1e-4

    def calc_from_real(self, positions_dict):
        if self.spectro._current_bragg_solution:
            bragg, soluce, reals_pos = self.spectro._current_bragg_solution
            for tag in positions_dict.keys():
                if abs(positions_dict[tag] - reals_pos[tag]) > self.tolerance:
                    bragg = numpy.nan
        else:
            bragg = numpy.nan

        return {"bragg": bragg}

    def calc_to_real(self, positions_dict):
        return self.spectro.compute_bragg_solution(positions_dict["bragg"])


class SpectroEnergyCalcController(CalcController):
    def __init__(self, spectro, cfg):
        super().__init__(cfg)
        self.spectro = spectro

    def calc_from_real(self, positions_dict):
        return {"energy": self.spectro.bragg2energy(positions_dict["bragg"])}

    def calc_to_real(self, positions_dict):
        return {"bragg": self.spectro.energy2bragg(positions_dict["energy"])}


class SpectroPlot:
    def __init__(self, spectro):
        self.spectro = spectro
        self._min_refresh_time = 0.2
        self._last_refresh_time = time.time()
        self.create_plot()

    def create_plot(self):
        self.plot = get_flint().get_plot(
            "spectroplot", "spectrotitle", "spectroid", selected=True
        )
        d = self.spectro.surface_radius_sagittal
        self.plot.set_box_min_max([-d, -d, -d], [d, d, d])
        self.plot.set_data(**self.spectro._get_plot_data())

    def is_active(self):
        return self.plot.is_open()

    def update_plot(self, forced=False):
        if self.plot.is_open():
            now = time.time()
            dt = now - self._last_refresh_time
            if forced or dt >= self._min_refresh_time:
                self._last_refresh_time = now
                self.plot.set_data(**self.spectro._get_plot_data())
