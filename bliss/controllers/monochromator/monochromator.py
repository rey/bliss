# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from importlib import import_module
import math
import functools
import copy
import numpy as np
import xcalibu
import tabulate

from bliss.common.types import IterableNamespace
from bliss.common.utils import autocomplete_property

from bliss.config import settings
from bliss.physics.units import ur, units
from bliss.physics.diffraction import CrystalPlane, _get_all_crystals, MultiPlane, hc
from bliss.controllers.bliss_controller import BlissController

"""
    This file contain classes which represent a set of common functionalities
    for monochromator control.
    
    We assume that a monochromator is composed of:
        - Rotation motor (bragg angle - real motor)
        - Energy motor (Calc Motor)
        - Crystal(s)
        
    This is represented by 3 classes MonochromatorBase, XtalManager and EnergyCalcMotor
    
    MonochromatorBase:
        * Minimum YAML file
            - plugin: bliss
              package: bm23.MONO.BM23monochromator
              class: MonochromatorBase
              name: bm23mono
              xtals: $bm23_mono_xtals       <-- see XtalManager section
              energy_motor: $bm23energy     <-- see EnergyCalcMotor section
              bragg_motor: $bm23bragg       <-- Bragg angle (real motor)
        
        * a Monochromator object as a name that you may use to get its status
            SESSION_MONO [1]: bm23mono
                     Out [1]: Monochromator: bm23mono
                              Crystal: Si111 [Si111 / Si333 / Si311 / Si511]
                              Energy: 11.387 KeV
                              Angle:  10.0 deg
        * For DCM monochromator, a fix exit offset can be given and passed
          to the XtalManager object (see XTalManager section)
            - YAML file
                - package: bm23.MONO.BM23mono
                  class: BM23Mono
                  name: bm23mono
                  xtals: $bm23_mono_xtals
                  energy_motor: $bm23energy
                  bragg_motor: $bm23bragg
                  fix_exit_offset: 25.0     <--
        * If the monochromator holds a set of crystals, a method to move
          from one crystal to the other(s) is foreseen. 
          In this case, a new class should be written which inherits
          from MonochromatorBase Class.
            - 3 methods should be re-written:
                + initialize(self)
                  Allow to get the necessary parameters in the mono yaml file
                + _xtal_change(self, xtal)
                  Move "xtal" in the beam
                + _xtal_is_in(self, xtal)
                  return True if "xtal" is in the beam, False otherwise
            - Additional parameters should also be added in the XtalManager
              YAML file to get, for example the position of the crystals in the beam
              
            - Example:
                + Mono YAML file example
                    - plugin: bliss
                      package: bm23.MONO.BM23mono
                      class: BM23Mono
                      name: bm23mono
                      xtals: $bm23_mono_xtals
                      energy_motor: $bm23energy
                      bragg_motor: $bm23bragg
                      change_motor: $smty
                      fix_exit_offset: 25.0
                
                + XtalManager file example
                
                + New Monochromator class
                
                    from bm23.MONO.BM23monochromator import MonochromatorBase
                    
                    class BM23Mono(MonochromatorBase):
    
                        def initialize(self):
                            # motor to move crystals in the beam
                            self.change_motor = self.config.get("change_motor")
                        
                            # position of each crystals in the beam
                            self.position = {}
                            xtals = self.xtals.config.get("xtals")
                            for xtal_index in range(len(xtals)):
                                xtal_name = xtals[xtal_index]["xtal"]
                                self.position[xtal_name] = float(xtals[xtal_index]["position"])

                        def _xtal_is_in(self, xtal):
                            if self.change_motor.position == self.position[xtal]:
                                return True
                            return False
        
                        def _xtal_change(self, xtal):
                            self.change_motor.move(self.position[xtal])
        

    
    XtalManager
        * Description of the crystal(s) mounted on the monochromator.
        * At least one crystal need to be defined ...
            - YAML file:
                plugin: bliss
                package: bm23.MONO.BM23monochromator
                class: XtalManager
                name: bm23_mono_xtals
                xtals:
                    - xtal: Si111
            - Available method:
                + bragg2energy
                + energy2bragg
        * ... but more may be used:
            - YAML file:
                xtals:
                    - xtal: Si111
                    - xtal: Si311  <--
                    - xtal: Ge511  <--
        * More methods are available for DCM monochromator, 
          if the "fix_exit_offset" attribute is set:
                + bragg2dxtal
                + energy2dxtal
                + dxtal2bragg
            - YAML file:            
                plugin: bliss
                package: bm23.MONO.BM23monochromator
                class: XtalManager
                name: bm23_mono_xtals
                fix_exit_offset: -10    <--
                xtals:
                    - xtal: Si111
                    - xtal: Si311
                    - xtal: Ge511
            
        * If you want to use crystal harmonics, add a crystal with its
          corresponding HKL values. You will be able to use it by changing 
          the selected crystal with the Monochromator object (mono.xtal_change(new_xtal)).
            - YAML file:
                xtals:
                    - xtal: Si111
                    - xtal: Si333  -> Use third harmonic of Si111
                    - xtal: Si311
                    - xtal: Ge511  
        * Calculations are using the module bliss.physics.diffraction based
          on the public module mendeleev which define a number of crystals parameters.
          You may specify your own dspacing (in Angstrom) in the yml file.
          In this case, this value MUST be specified for each crystal
            - YAML file:
                xtals:
                    - xtal: Si111
                      dspacing: 3.1356  --> new dspacing for Si111
                    - xtal: Si333
                      dspacing: 3.1356  --> need to be set to be coherent
                    - xtal: Ge511       --> using default constants
        * Multilayers: 
            + Principle
              - the Bragg to Energy calculation for a multilayer is given by:
                n*lambda = 2*d-spacing*sqrt(1-2*delta_bar/pow(sin(theta),2))*sin(theta)
              - n = order of reflexion
              - d-spacing of such a multilayer is the sum of the thickness
                of each material (in Angstrom in the calculation)
              - delta_bar:
                . Parameter which is energy dependant.
                . Given by the Optic Group as a file Energy/Delta_bar
                . this formula is not bijective. In consequence a lookup table
                  is built at the creation of the object to get energy
                  from angle or angle from energy.
            
            + Configuration
                + to define a multilayer, use the tag "multilayer" instead of "xtal"
                + dpspacing:
                  . Use tag "thickness1" and "thickness2" if the materials and their
                    thicknesses are known. Only 2 materials are taken into account.
                    Thickness must be given in nm
                  . Use tag "dspacing" if the materials are unknown but you know
                    an approximate dspacing.
                    dspacing must be given in nm
                + delta_bar:
                  . Use tag "delta_bar" to specify the file given by the optic group
                  . if delta_bar is not specified, delta_bar=0
                + lookup table:
                  . if none of the parameters are known you can directly specify a
                    lookup table energy(eV) vs bragg angle (radian)
                  . Use the tag "lookup_table" to specify the file containing the lut
                  
            + YAML File:
                xtals:
                  - xtal: Si111       <-- Normal crystal definition
      
                  - multilayer: ML_1  <-- Multilayer: All parameters are known
                    dspacing1: 10        
                    dspacing1: 11    
                    delta_bar: /users/blissadm/local/beamline_configuration/mono/multilayer/OpticalConstantsW_B4C.txt
      
                  - multilayer: ML_2  <-- Multilayer: Only an approximate dspacing is known
                    dspacing: 21                      delta_bar = 0
      
                  - multilayer: ML_2  <-- Multilayer: Only a Energy<-> Bragg lookup table is given
                    lookup_table: /users/blissadm/local/beamline_configuration/mono/multilayer/Bm29_multilayer.txt
        
            
    EnergyCalcMotor
        * EnergyCalcMotor need to know the Monochromator it is refering to.
          At Initialization of the mono, the method EnergyCalcMotor.set_mono(self)
          is called.
          The Value of the EnergyCalcMotor is Nan before this call or if no crystal
          is selected in the XtalManager object. The Monochromator object 
          is in charge to do this selection.
        * YAML file
            - plugin: emotion
              package: bm23.MONO.BM23monochromator
              class: EnergyCalcMotor
              axes:
                - name: $id10bragg
                  tags: real bragg
                - name: id10energy
                  tags: energy
                  unit: KeV

"""


class MonochromatorBase(BlissController):
    """
    Monochromator Base
    """

    def __init__(self, config):

        super().__init__(config)
        self.__settings = None

        self._trackers = self.config.get("trackers")
        # tracking objects
        if self._trackers:
            trackers = {tracker.name: tracker for tracker in self._trackers}
            for tracker in trackers.values():
                tracker.mono = self
            self._trackers = trackers

        # bragg motor
        self.motors = {}
        self.motors["bragg"] = self.config.get("bragg_motor")
        self.bragg_motor = self.motors["bragg"]

        if not self.motors["bragg"].unit:
            raise RuntimeError("Should specify unit for the bragg motor")

        # create calc controllers now (before _load_config)
        self._enecc = self._create_ene_calc_mot()
        self.motors["ene"] = self._enecc.get_axis(self.config["energy_motor"]["name"])
        self.motors["ene"].controller.set_mono(self)
        self.energy_motor = self.motors["ene"]

        if self.config.get("energy_track_motor"):
            self._enetrackcc = self._create_enetrack_calc_mot()
            self.motors["ene_track"] = self._enetrackcc.get_axis(
                self.config["energy_track_motor"]["name"]
            )
            self.motors["ene_track"].controller.set_mono(self)
            self.energy_track_motor = self.motors["ene_track"]
        else:
            self.motors["ene_track"] = None

        self.track_tables = TrackTableMulti(lambda: self.tracked_axes)

    def _load_config(self):
        # xtals
        self.xtals = self.config.get("xtals", None)
        if self.xtals is None:
            raise RuntimeError("No XtalManager configured")
        if len(self.xtals.xtal_names) == 0:
            raise RuntimeError("No Crystals Defined in the XtalManager")

        # Add label-named method for all positions.
        for xtal in self.xtals.xtal_names:
            self._add_label_move_method(xtal)

    def _init(self):
        xtal = self.xtals.xtal_sel
        if xtal is not None:
            if not self.xtal_is_in(xtal):
                xtal_in = self.xtal_in()
                if len(xtal_in) == 0:
                    self.xtals.xtal_sel = None
                else:
                    self.xtals.xtal_sel = xtal_in[0]
        else:
            for xtal in self.xtals.xtal_names:
                if self.xtal_is_in(xtal):
                    self.xtals.xtal_sel = xtal

    @autocomplete_property
    def axes(self):
        return IterableNamespace(**self.motors)

    @autocomplete_property
    def trackers(self):
        return IterableNamespace(**self._trackers)

    def _get_item_owner(self, name, cfg, pkey):
        """Return the container that owns the items declared in the config.
        By default, this container is the owner of all items.
        However if this container has sub-containers, use this method to specify
        the items owners.
        """
        if pkey == "energy_motor":
            return self._enecc
        if pkey == "energy_track_motor":
            return self._enetrackcc
        return self

    def _create_subitem_from_config(self, name, cfg, parent_key, item_class, item_obj):
        # Called when a new subitem is created (i.e accessed for the first time via self._get_subitem)
        """
        Return the instance of a new item owned by this controller.

        args:
            name: item name  (or instance of a referenced object if item_class is None)
            cfg : item config
            parent_key: the config key under which the item was found (ex: 'counters').
            item_class: a class to instantiate the item (=None for referenced item)

        return: item instance

        """
        if parent_key == "energy_motor":
            return self._enecc.get_axis(name)
        if parent_key == "energy_track_motor":
            return self._enetrackcc.get_axis(name)
        raise NotImplementedError

    def __get_class_from_str(self, class_name):
        if "." in class_name:  # from absolute path
            idx = class_name.rfind(".")
            module_name, cname = class_name[:idx], class_name[idx + 1 :]
            module = __import__(module_name, fromlist=[""])
            return getattr(module, cname)
        module = import_module(
            "bliss.controllers.monochromator"
        )  # try at the controller module level first
        if hasattr(module, class_name):
            return getattr(module, class_name)
        raise ModuleNotFoundError(f"cannot find class {class_name} in {module}")

    def _create_ene_calc_mot(self):
        ene_mot_name = self.config["energy_motor"]["name"]

        # Energy Calc Motor config is like:
        #
        # - plugin: emotion
        #   module: monochromator
        #   class: EnergyCalcMotor
        #   axes:
        #     - name: $sim_mono
        #       tags: real bragg
        #     - name: ene
        #       tags: energy
        #       unit: keV

        bragg_mot = self.config.get("bragg_motor")

        ene = {
            "axes": [
                {"name": bragg_mot, "tags": "real bragg"},
                {
                    "name": ene_mot_name,
                    "tags": "energy",
                    "unit": self.config["energy_motor"]["unit"],
                },
            ]
        }

        klass = self.config["energy_motor"].get("ctrl_class")
        if klass is not None:
            klass = self.__get_class_from_str(klass)
        else:
            from bliss.controllers.monochromator import EnergyCalcMotor as klass

        enecc = klass("enecc", ene)
        enecc._initialize_config()
        return enecc

    def _create_enetrack_calc_mot(self):
        enetrack_mot_name = self.config["energy_track_motor"]["name"]

        # EnergyTrack CalcMotor config is like:
        #
        # - plugin: emotion
        #   module: monochromator
        #   class: EnergyTrackingCalcMotor
        #   approximation: 0.0005
        #   axes:
        #     - name: $ene
        #       tags: real energy
        #     - name: $sim_c1
        #       tags: real sim_c1
        #     - name: $sim_c2
        #       tags: real sim_c2
        #     - name: $sim_acc
        #       tags: real sim_acc
        #     - name: enetrack
        #       tags: energy_track
        #       unit: keV

        enetrack = {
            "axes": [
                {"name": self.energy_motor, "tags": "real energy"},
                {
                    "name": enetrack_mot_name,
                    "tags": "energy_track",
                    "unit": self.config["energy_track_motor"]["unit"],
                },
            ]
        }

        # add real motors to config from trackers
        for tracker in self.trackers:
            for axis in tracker.motors.values():
                real = dict()
                real.update({"name": axis, "tags": "real " + axis.name})
                enetrack["axes"].append(real)

        klass = self.config["energy_track_motor"].get("ctrl_class")
        if klass is not None:
            klass = self.__get_class_from_str(klass)
        else:
            from bliss.controllers.monochromator import EnergyTrackingCalcMotor as klass

        enetrackcc = klass("enetrackcc", enetrack)
        enetrackcc._initialize_config()
        return enetrackcc

    def close(self):
        self.__close__()

    def __close__(self):
        for motor in self.motors.values():
            if hasattr(motor.controller, "close"):
                motor.controller.close()

    @property
    def settings(self):
        return self.__settings

    def __info__(self):
        info_str = self._info_mono()
        info_str += self._info_xtals()
        info_str += self._info_motor()
        if self._trackers:
            info_str += self._info_trackers()
        return info_str

    def _info_mono(self):
        info_str = f"Monochromator: {self.name}\n\n"
        return info_str

    def _info_xtals(self):
        info_str = ""
        if not self.xtals:
            return info_str
        unit = self.motors["bragg"].unit
        info_str += self.xtals.__info__()
        if "deg" in info_str and unit == "deg":
            return info_str

        # there is a need to change the units and the numbers for the bragg
        info_str = info_str.replace("deg", unit)
        return info_str

    def _info_trackers(self):
        info_str = ""
        for tracker in self.trackers:
            info_str += f"\n\n{tracker.name.upper()}\n"
            info_str += tracker.__info__()
        return info_str

    def _info_motor(self):
        bragg = self.motors["bragg"].position
        unit = self.motors["bragg"].unit or "deg"
        value = (bragg * ur.parse_units(unit)).to("deg").magnitude
        energy = self.bragg2energy(value)
        #
        # TITLE
        #
        title = [""]
        title.append(self.motors["bragg"].name)
        if "bragg_fe" in self.motors:
            title.append(self.motors["bragg_fe"].name)
        title.append(self.motors["ene"].name)
        if self.motors["ene_track"] is not None:
            title.append(self.motors["ene_track"].name)

        #
        # CALCULATED POSITION ROW
        #
        calculated = ["Calculated"]
        # bragg
        val = self.motors["bragg"].position
        valu = self.motors["bragg"].unit
        calculated.append(f"{val:.3f} {valu}")
        # bragg_fe
        if "bragg_fe" in self.motors:
            val = self.motors["bragg_fe"].position
            valu = self.motors["bragg_fe"].unit
            calculated.append(f"{val:.3f} {valu}")
        # energy
        valu = self.motors["ene"].unit
        calculated.append(f"{energy:.3f} {valu}")
        # tracker
        if self.motors["ene_track"] is not None:
            valu = self.motors["ene_track"].unit
            calculated.append(f"{energy:.3f} {valu}")

        #
        # CURRENT POSITION ROW
        #
        current = ["Current"]
        # bragg
        val = self.motors["bragg"].position
        valu = self.motors["bragg"].unit
        current.append(f"{val:.3f} {valu}")
        # bragg_fe
        if "bragg_fe" in self.motors:
            val = self.motors["bragg_fe"].position
            valu = self.motors["bragg_fe"].unit
            current.append(f"{val:.3f} {valu}")
        # energy
        valu = self.motors["ene"].unit
        val = self.motors["ene"].position
        current.append(f"{val:.3f} {valu}")
        # tracker
        if self.motors["ene_track"] is not None:
            val = self.motors["ene_track"].position
            valu = self.motors["ene_track"].unit
            current.append(f"{val:.3f} {valu}")

        mystr = tabulate.tabulate(
            [calculated, current], headers=title, tablefmt="plain", stralign="right"
        )

        return "\n" + mystr

    """
    For SPEC compatibility:
    This method change the offset of the Bragg motor to fit with an energy
    which has been positioned using a known sample.
    Remarks:
        - The mono need to be at the given energy.
        - In case of the bragg motor being a CalcMotor, do not forget
          to foresee the set offset method in it.
    """

    def setE(self, energy):
        new_bragg_pos = self.energy2bragg(energy)
        mot = self.motors["bragg"]
        mot.position = new_bragg_pos
        self.motors["ene"].update_position()

    ###############################################################
    ###
    ### Xtals
    ###
    @property
    def xtal_sel(self):
        return self.xtals.xtal_sel

    @xtal_sel.setter
    def xtal_sel(self, xtal):
        self.xtals.xtal_sel = xtal

    """
    In case of monochromator with multi crystals a changer may be implemented
    By default, each crystal is considered in place. Changing the crystal only
    change the energy calculation
    """

    def xtal_is_in(self, xtal):
        if xtal in self.xtals.xtal_names:
            return self._xtal_is_in(xtal)
        raise RuntimeError(f"Crystal {xtal} not configured")

    def _xtal_is_in(self, xtal):
        """
        This method needs to be overwritten to reflect the monochromator behaviour
        """
        return True

    def _add_label_move_method(self, xtal_name):
        """Add a method named after the xrtal najme to move to the
        corresponding position.
        Args:
        xtal_name (str): Chrystal or layer name.
        """

        def label_move_func(pos):
            print(f"Moving to: {pos}")
            self.xtal_change(pos)

        # name should not start with a number!
        if xtal_name.isidentifier():
            setattr(self, xtal_name, functools.partial(label_move_func, pos=xtal_name))
        else:
            log_error(
                self, f"{self.name}: '{xtal_name}' is not a valid python identifier."
            )

    def xtal_change(self, xtal):
        if xtal in self.xtals.xtal_names:
            self._xtal_change(xtal)
            self.xtals.xtal_sel = xtal
            self.motors["ene"].sync_hard()
        else:
            raise RuntimeError(f"Crystal {xtal} not configured")

    def _xtal_change(self, xtal):
        """
        This method needs to be overwritten to reflect the monochromator behaviour
        """
        pass

    def xtal_in(self):
        """
        return the list of crystals in place
        """
        xtal_in_place = []
        for xtal in self.xtals.xtal_names:
            if self.xtal_is_in(xtal):
                xtal_in_place.append(xtal)
        return xtal_in_place

    ################################################################
    ###
    ### Calculation methods
    ###
    def energy2bragg(self, ene):
        return self.xtals.energy2bragg(ene)

    def bragg2energy(self, bragg):
        return self.xtals.bragg2energy(bragg)

    ################################################################
    ###
    ### Tracking
    ###
    def track_info(self):
        if self.tracked_axes is None:
            print("No Tracking defined for this monochromator")
            return

        print("")

        for tracker in self.trackers:
            print(f"  {tracker.name.upper()}")
            tracker.track_info()

    def track_on(self, *axes):
        if self.motors["ene_track"] is None:
            raise ValueError("No Tracking defined for this monochromator")
        for tracker in self.trackers:
            tracker.track_on(*axes)

    def track_off(self, *axes):
        if self.motors["ene_track"] is None:
            raise ValueError("No Tracking defined for this monochromator")
        for tracker in self.trackers:
            tracker.track_off(*axes)

    # def is_tracked(self, axis):
    #     if self.motors["ene_track"] is None:
    #         raise ValueError("No Tracking defined for this monochromator")

    #     cont = self.motors["ene_track"].controller
    #     if axis in cont.reals:
    #         tag = cont._axis_tag(axis)
    #         if tag != "energy":
    #             return axis.track

    @property
    def tracked_axes(self):
        if self.motors["ene_track"] is None:
            return None

        cont = self.motors["ene_track"].controller
        tracked_list = []
        for axis in cont.reals:
            tag = cont._axis_tag(axis)
            if tag != "energy" and axis.track:
                tracked_list.append(axis)
        return tracked_list

    def track_mode(self, axis, mode=None):
        if self.motors["ene_track"] is not None:
            cont = self.motors["ene_track"].controller
            if axis in cont.reals:
                tag = cont._axis_tag(axis)
                if tag != "energy":
                    if mode is None:
                        return axis.track_mode
                    axis.track_mode = mode
        return None


class MonochromatorFixExitBase(MonochromatorBase):
    def __init__(self, name, config):

        MonochromatorBase.__init__(self, name, config)

        # CalcMotor which manage Real Bragg axis and Xtal motor
        self.motors["bragg_fe"] = self.config.get("bragg_fe_motor", None)
        if self.motors["bragg_fe"] is None:
            raise RuntimeError(
                'MonochromatorFixExitBase class need to reference a "bragg_fe_motor"'
            )
        self.motors["bragg_fe"].controller.set_mono(self)
        cont = self.motors["bragg_fe"].controller
        for axis in cont.reals:
            if cont._axis_tag(axis) == "xtal":
                self.motors["xtal"] = axis

        # Fix exit Geometry
        self.__fix_exit_offset = self.config.get("fix_exit_offset", None)
        if self.fix_exit_offset is None:
            raise RuntimeError(
                f"Monochromator {self.name} do not define fix_exit_offset"
            )

    def __info__(self):
        info_str = self._info_mono()
        info_str += self._info_xtals()
        info_str += self._info_fix_exit()
        info_str += self._info_motor()
        return info_str

    def _info_fix_exit(self):
        info_str = f"Fix exit_offset: {self.fix_exit_offset}\n\n"
        return info_str

    @property
    def fix_exit_offset(self):
        return self.__fix_exit_offset

    @fix_exit_offset.setter
    def fix_exit_offset(self, val):
        self.__fix_exit_offset = val

    def bragg2dxtal(self, bragg):
        """
        Double Xtal Fix Exit Monochromator specific
        """
        if self.fix_exit_offset is not None:
            dxtal = self.fix_exit_offset / (2.0 * np.cos(np.radians(bragg)))
            return dxtal
        raise RuntimeError("No Fix Exit Offset defined")

    def dxtal2bragg(self, dxtal):
        if self.fix_exit_offset is not None:
            bragg = np.degrees(np.arccos(self.fix_exit_offset / (2.0 * dxtal)))
            return bragg
        raise RuntimeError("No Fix Exit Offset defined")

    def energy2dxtal(self, ene):
        bragg = self.energy2bragg(ene)
        dxtal = self.bragg2dxtal(bragg)
        return dxtal

    def dxtal2xtal(self, dxtal):
        raise NotImplementedError

    def xtal2dxtal(self, xtal):
        raise NotImplementedError

    def bragg2xtal(self, bragg):
        dxtal = self.bragg2dxtal(bragg)
        xtal = self.dxtal2xtal(dxtal)
        return xtal

    def xtal2bragg(self, xtal):
        dxtal = self.xtal2dxtal(xtal)
        bragg = self.dxtal2bragg(dxtal)
        return bragg

    def energy2xtal(self, ene):
        bragg = self.energy2bragg(ene)
        dxtal = self.bragg2dxtal(bragg)
        xtal = self.dxtal2xtal(dxtal)
        return xtal


"""
    Crystals management + Energy Calculation
    
    YML file example
        - plugin: bliss
          package: bm23.MONO.BM23monochromator
          class: XtalManager
          name: mono_xtals
          xtals:
            - xtal: Si111
            - xtal: Si333
            - xtal: Ge311

"""


class XtalManager:
    def __init__(self, name, config):

        self.__name = name
        self.__config = config

        # Crystal(s) management
        self.all_xtals = self.get_all_xtals()
        xtals = self.config.get("xtals")

        self.xtal_names = []
        self.xtal = {}
        for elem in xtals:
            if "xtal" in elem.keys():
                xtal_name = elem.get("xtal")
                dspacing = elem.get("dspacing", None)
                symbol = self.xtalplane2symbol(xtal_name)
                if symbol not in self.all_xtals:
                    if dspacing is not None:
                        self.xtal[xtal_name] = MultiPlane(distance=dspacing * 1e-10)
                    else:
                        raise RuntimeError("dspacing of Unknown crystals must be given")
                else:
                    self.xtal[xtal_name] = copy.copy(CrystalPlane.fromstring(xtal_name))
                if dspacing is not None:
                    self.xtal[xtal_name].d = dspacing * 1e-10
                self.xtal_names.append(xtal_name)
            elif "multilayer" in elem.keys():
                ml_name = elem.get("multilayer")
                self.xtal[ml_name] = Multilayer(ml_name, elem)
                self.xtal_names.append(ml_name)

        def_val = {"xtal_sel": None}
        self.__settings_name = f"XtalManager_{self.name}"
        self.__settings = settings.HashSetting(
            self.__settings_name, default_values=def_val
        )
        if self.settings["xtal_sel"] not in self.xtal_names:
            self.settings["xtal_sel"] = None

    @property
    def name(self):
        return self.__name

    @property
    def config(self):
        return self.__config

    @property
    def settings(self):
        return self.__settings

    @property
    def xtal_sel(self):
        return self.settings["xtal_sel"]

    @xtal_sel.setter
    def xtal_sel(self, xtal):
        if xtal is None or xtal in self.xtal_names:
            self.settings["xtal_sel"] = xtal
        else:
            raise RuntimeError(f"Crystal ({xtal}) not configured")

    def __info__(self):
        if self.xtal_sel is not None:
            xtal_sel = self.xtal[self.xtal_sel]
            if isinstance(xtal_sel, Multilayer):
                info_str = "Multilayer:"
            else:
                info_str = "Crystal:"
        else:
            info_str = "Crystal:"
        info_str += f" {self.xtal_sel} ["
        for xtal in self.xtal_names:
            info_str += f"{xtal} / "
        info_str = info_str[:-3] + "]"
        info_str += "\n"

        if self.xtal_sel is not None:
            if isinstance(xtal_sel, Multilayer):
                ml_str = xtal_sel.__info__()
                info_str += ml_str
            else:
                dspacing = (self.xtal[self.xtal_sel].d * ur.m).to("angstrom")
                info_str += f"dspacing: {dspacing:.5f}\n"

        return info_str

    #
    # Utils
    #

    def get_all_xtals(self):
        xtals = _get_all_crystals()
        all_xtals = []
        for xtal in xtals:
            all_xtals.append(xtal.name)
        return all_xtals

    def xtalplane2symbol(self, xtalplane):
        symbol, plane = "", ""
        for c in xtalplane:
            if c.isdigit() or c.isspace():
                plane += c
            elif c.isalpha():
                symbol += c
        return symbol

    def get_xtals_config(self, key):
        res = {}
        xtals = self.config.get("xtals")
        for elem in xtals:
            if "xtal" in elem.keys():
                elem_name = elem.get("xtal")
            elif "multilayer" in elem.keys():
                elem_name = elem.get("multilayer")
            else:
                raise RuntimeError('Neither "xtal" nor "multilayer" keyword in xtal')
            res[elem_name] = float(elem.get(key))

        return res

    #
    # Calculation methods
    #

    def energy2bragg(self, ene):
        """Calculate the bragg angle as function of the energy.
        Args:
            ene(float): Energy [keV]
        Returns:
            (float): Bragg angle value [deg]
        """
        if self.xtal_sel is None:
            return np.nan
        xtal = self.xtal[self.xtal_sel]
        bragg = xtal.bragg_angle(ene * ur.keV)
        if np.isnan(bragg).any():
            return np.nan

        # convert radians to degrees
        bragg = bragg.to(ur.deg).magnitude
        return bragg

    def bragg2energy(self, bragg):
        """Calculate the energy as function of the bragg angle
        Args:
            bragg(float): Bragg angle [deg]
        Returns:
            (float): Energy [keV]
        """
        if self.xtal_sel is None:
            return np.nan
        xtal = self.xtal[self.xtal_sel]
        energy = xtal.bragg_energy(bragg * ur.deg)
        if np.isnan(energy.magnitude).any():
            return np.nan
        energy = energy.to(ur.keV).magnitude
        return energy


class Multilayer:
    def __init__(self, name, config):

        self.__name = name
        self.__config = config

        self.thickness1 = self.config.get("thickness1", None)
        self.thickness2 = self.config.get("thickness2", None)
        self.ml_file = self.config.get("delta_bar", None)
        self.lut_file = None
        if self.thickness1 is not None and self.thickness2 is not None:
            self.d = ((self.thickness1 + self.thickness2) * 1e-9) * ur.m
            if self.ml_file is not None:
                self.create_lut_from_ml_file()
        else:
            dspacing = self.config.get("dspacing", None)
            if dspacing is not None:
                self.d = (dspacing * 1e-9) * ur.m
            else:
                self.d = None
                self.lut_file = self.config.get("lookup_table", None)
                if self.lut_file is not None:
                    self.create_lut_from_lut_file()
                else:
                    raise RuntimeError(f"Multilayer {name}: Wrong yml configuration")

    @property
    def name(self):
        return self.__name

    @property
    def config(self):
        return self.__config

    def __info__(self):
        info_str = ""
        if self.thickness1 is not None and self.thickness2 is not None:
            info_str += f"Thickness Material #1: {self.thickness1*ur.nm}\n"
            info_str += f"Thickness Material #2: {self.thickness2*ur.nm}\n"
            dspacing = self.d.to("nm")
            info_str += f"d-spacing: {dspacing}\n"
            if self.ml_file is not None:
                min_en = (self.en2bragg.min_x() * ur.J).to("keV").magnitude
                max_en = (self.en2bragg.max_x() * ur.J).to("keV").magnitude
                min_th = np.degrees(self.en2bragg.min_y())
                max_th = np.degrees(self.en2bragg.max_y())

                info_str += f"delta_bar file: {self.ml_file}\n"
                info_str += f"Energy (keV): [{min_en:.3f} : {max_en:.3f}]\n"
                info_str += f"Bragg (deg) : [{max_th:.3f} : {min_th:.3f}]\n"
        else:
            if self.d is not None:
                dspacing = self.d.to("nm")
                info_str += f"d-spacing: {dspacing}\n"
            else:
                if self.lut_file is not None:
                    min_en = (self.en2bragg.min_x() * ur.J).to("keV").magnitude
                    max_en = (self.en2bragg.max_x() * ur.J).to("keV").magnitude
                    min_th = np.degrees(self.en2bragg.min_y())
                    max_th = np.degrees(self.en2bragg.max_y())

                    info_str += f"Lookup table file: {self.lut_file}\n"
                    info_str += f"Energy [keV]: [{min_en:.3f} : {max_en:.3f}]\n"
                    info_str += f"Bragg [deg]: [{max_th:.4f} : {min_th:.4f}]\n"

                else:
                    raise RuntimeError("THIS ERROR SHOULD NEVER HAPPENED !!!\n")

        return info_str

    def create_lut_from_ml_file(self):
        if self.ml_file is not None:
            arr = np.loadtxt(self.ml_file, comments="#").transpose()
            arr_energy = np.copy((arr[0] * ur.keV).to(ur.J))
            arr_theta = np.arcsin(
                np.sqrt(arr[5] + np.power(hc / (2.0 * self.d * arr_energy), 2))
            )

            self.en2bragg = xcalibu.Xcalibu()
            self.en2bragg.set_calib_name(f"{self.name}_bragg")
            self.en2bragg.set_calib_time(0)
            self.en2bragg.set_calib_type("TABLE")
            self.en2bragg.set_reconstruction_method("INTERPOLATION")
            self.en2bragg.set_raw_x(arr_energy.magnitude)
            self.en2bragg.set_raw_y(arr_theta.magnitude)

            arr_flip_theta = np.flip(arr_theta)
            arr_flip_energy = np.flip(arr_energy)
            self.bragg2en = xcalibu.Xcalibu()
            self.bragg2en.set_calib_name(f"{self.name}_bragg")
            self.bragg2en.set_calib_time(0)
            self.bragg2en.set_calib_type("TABLE")
            self.bragg2en.set_reconstruction_method("INTERPOLATION")
            self.bragg2en.set_raw_x(arr_flip_theta.magnitude)
            self.bragg2en.set_raw_y(arr_flip_energy.magnitude)

    def create_lut_from_lut_file(self):
        """Create a Lookup table from a file. The file should be in format of
        two columns: Energy [eV] bragg_angle [rad]
        """
        if self.lut_file is not None:
            arr = np.loadtxt(self.lut_file, comments="#").transpose()
            arr_energy = np.copy(((arr[0] / 1000.0) * ur.keV).to(ur.J))
            arr_theta = np.copy(arr[1] * ur.radians)

            self.en2bragg = xcalibu.Xcalibu()
            self.en2bragg.set_calib_name(f"{self.name}_bragg")
            self.en2bragg.set_calib_time(0)
            self.en2bragg.set_calib_type("TABLE")
            self.en2bragg.set_reconstruction_method("INTERPOLATION")
            self.en2bragg.set_raw_x(arr_energy.magnitude)
            self.en2bragg.set_raw_y(arr_theta.magnitude)

            arr_flip_theta = np.flip(np.copy(arr_theta))
            arr_flip_energy = np.flip(np.copy(arr_energy))
            self.bragg2en = xcalibu.Xcalibu()
            self.bragg2en.set_calib_name(f"{self.name}_bragg")
            self.bragg2en.set_calib_time(0)
            self.bragg2en.set_calib_type("TABLE")
            self.bragg2en.set_reconstruction_method("INTERPOLATION")
            self.bragg2en.set_raw_x(arr_flip_theta.magnitude)
            self.bragg2en.set_raw_y(arr_flip_energy.magnitude)

    @units(wavelength="m", result="J")
    def wavelength_to_energy(self, wavelength):
        """
        Returns photon energy (J) for the given wavelength (m)

        Args:
            wavelength (float): photon wavelength (m)
        Returns:
            float: photon energy (J)
        """
        try:
            return hc / wavelength
        except ZeroDivisionError:
            raise ZeroDivisionError("Cannot calculate energy for bragg angle = 0")

    @units(energy="J", result="m")
    def energy_to_wavelength(self, energy):
        """
        Returns photon wavelength (m) for the given energy (J)

        Args:
            energy (float): photon energy (J)
        Returns:
            float: photon wavelength (m)
        """
        return hc / energy

    @units(theta="rad", result="m")
    def bragg_wavelength(self, theta, n=1):
        """
        Return a bragg wavelength (m) for the given theta and distance between
        lattice planes.

        Args:
            theta (float): scattering angle (rad)
            n (int): order of reflection. Non zero positive integer (default: 1)
        Returns:
            float: bragg wavelength (m) for the given theta and lattice distance
        """
        return 2.0 * self.d * math.sin(theta)

    @units(theta="rad", result="J")
    def bragg_energy(self, theta, n=1):
        """
        Return a bragg energy for the given theta and distance between lattice
        planes.

        Args:
            theta (float): scattering angle (rad)
            n (int): order of reflection. Non zero positive integer (default: 1)
        Returns:
            float: bragg energy (J) for the given theta and lattice distance
        """
        if self.ml_file is None and self.lut_file is None:
            return self.wavelength_to_energy(self.bragg_wavelength(theta, n=n))
        th = theta.magnitude
        if th >= self.bragg2en.min_x() and th <= self.bragg2en.max_x():
            return self.bragg2en.get_y(theta.magnitude) * ur.J
        return (np.nan) * ur.J

    @units(energy="J", result="rad")
    def bragg_angle(self, energy):
        """
        Return a bragg angle [rad] for the given theta and distance between
        lattice planes.

        Args:
            energy (float): energy [J]
            d (float): interplanar distance between lattice planes [m]
            n (int): order of reflection. Non zero positive integer (default: 1)
        Returns:
            (float): bragg angle [rad] for the given theta and lattice distance
        """
        if self.ml_file is None and self.lut_file is None:
            return np.arcsin(hc / (2.0 * self.d * energy))
        en = energy.magnitude
        if en >= self.en2bragg.min_x() and en <= self.en2bragg.max_x():
            return self.en2bragg.get_y(energy.magnitude) * ur.rad
        return (np.nan) * ur.rad


class TrackTableMulti:
    def __init__(self, tracked_axes):
        self.__tracked_axes = tracked_axes

    def plot(self, axis):
        """display table points for given axis on a plot"""
        if isinstance(axis, str):
            axis_name = axis
        else:
            axis_name = axis.name
        for axs in self.__tracked_axes():
            if axs.name == axis_name:
                return axs.track_table.plot()
        return None

    def __info__(self):
        """print the calib tables in use for all tracked axes"""
        axes = [axis for axis in self.__tracked_axes() if axis.track_mode == "table"]
        if len(axes) == 0:
            return "No axis currently tracked with table mode"
        title = ["Energy"] + [axis.name for axis in axes]

        energies = [axis.track_table.calib.x_raw for axis in axes]
        energies = np.unique(np.concatenate(energies))

        def position(axis, energy):
            try:
                return axis.track_table.calib.get_y(energy)
            except Exception:
                return None

        data = []
        for energy in energies:
            data.append([energy] + [position(axis, energy) for axis in axes])

        mystr = tabulate.tabulate(data, headers=title)

        return "\n" + mystr

    def setpoint(self):
        """add current position (energy, motor) to table for all tracked axes"""
        for axis in self.__tracked_axes():
            axis.track_table.setpoint()

    def delpoint(self, energy=None):
        """delete current energy from table for all tracked axes"""
        for axis in self.__tracked_axes():
            axis.track_table.delpoint(energy)

    def save(self):
        """save table calib files to beamline configuration for all tracked axes"""
        for axis in self.__tracked_axes():
            axis.track_table.save()
