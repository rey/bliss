
Elettra refers to picoammeters designed at Elettra and
commercialized by CAEN CAENels

The models this controller deals with are:
* AH501D aka epicea ePicea
* AH501B

Only AH501 models have been tested so far.
AH401 and TetrAMM not supported yet.


## Configuration

This section explains how to configure the Elettra electrometer to get BPM
values as counters.

### YAML configuration example

```yaml

- class: Elettra
  module: tango_elettra
  name: el2
  uri: //id42ctrl2:20000/id42/elettra/ss1
  counters:
  - counter_name: el2x
    measure: Y
  - counter_name: el2y
    measure: Z
  - counter_name: el2i
    measure: integration_time
  - counter_name: el2n
    measure: samples_number
  - counter_name: c1
    measure: current1
  - counter_name: c2
    measure: current2
  - counter_name: c3
    measure: current3
  - counter_name: c4
    measure: current4
  - counter_name: ctot
    measure: current_total
```

### Tango Server

This controller requires the `elettraAH` Tango server to work.

To install the server, use the package called `elettraAH` in blissinstaller.

A conda package will be provided in the next future.

http://wikiserv.esrf.fr/bliss/index.php/CAENels_picoammeter#Tango_Device_Server


## Usage

```
MCL [2]: el2
Out [2]: ePicea - model AH501B 1.0.0 - tango server //id20ctrl2:20000/id20/elettra/ss2

                   Full scale range: 0.0025 A
                   Measuring offset: [0. 0. 0. 0.] A

                   Last measurements:
                            current1: 4.419684410095215e-07 A
                            current2: -3.4582614898681642e-06 A
                            current3: -3.0305981636047363e-06 A
                            current4: -5.78761100769043e-06 A
                    integration time: 0.10 s
                   number of samples: 64
                                   X: -0.4902543439939562
                                   Y: -0.09660035255603118

                   The device is in RUNNING state.
```

To set the measuring range, give maximum current value you expect to measure in Amps:
```
MCL [15]: el2.range
Out [15]: 0.0024999999441206455
```

It is highly recommended to setup measurement offsets, for each different measuring
range. One can specify the integration time in seconds, default is 1. offset_reset()
command will clear the offset values.

```
MCL [17]: el2.offset_measure()
MCL [18]: el2.offset
Out [18]: array([ 4.37120973e-07, -3.46098937e-06, -3.03161606e-06, -5.79075649e-06])
MCL [19]: el2.offset_reset()
MCL [20]: el2.offset
Out [20]: array([0., 0., 0., 0.])
