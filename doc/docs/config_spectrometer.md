# Spectrometer

### Spectrometer geometry convention: 

**Points**:

    `S`: point of interaction between the sample and the beam (origin of spectrometer referential)

    `A0`: center of the central analyser  (virtual, A0_y = A0_z = 0)

    `D0`: center of the detector (D0_y = 0)

    `Ai`: center of analyser i

    `Di`: position on detector of the beam reflected by the analyser Ai


**Vectors**:

    `SA`: Sample to Analyser

    `SD`: Sample to Detector

    `NA`: normal to analyser surface on point A (optical)

    `NAc`: normal to analyser crystal plane on point A (diffraction)


**Planes**:

    `Meridional plane (MeP)`: defined by the 3 points < S, Ai, Di >

    `Scattering plane (ScP)`: contains SA and normal to MeP

    `Sagittal   plane (SgP)`: contains NA and normal to MeP


**Spectrometer referentials**:

    `Origin`: centred on S

    `+X`: SA0 / norm(SA0) (perpendicular to incoming beam and contained in the equatorial plane)

    `+Y`: colinear to incoming beam

    `+Z`: X ^ Y (pointing toward sky)


### YML Configuration

""" yml
spectrometers:

    - name: spectro
        plugin: generic
        module: spectrometers.spectrometer_base
        class: Spectrometer
        surface_radius_meridional: 5
        surface_radius_sagittal: 10
        crystal: Si111
        miscut: 0

        analysers:
        - name: a0
            angular_offset: 0 
            offset_on_detector: 0
            rpos: $xa0
            zpos: $za0
            pitch: $pita0
            yaw: $yawa0

        - name: a1
            angular_offset: 14 
            offset_on_detector: 0
            rpos: $xa1
            zpos: $za1
            pitch: $pita1
            yaw: $yawa1

        - name: a2
            # angular_offset: -14
            offset_on_detector: 0
            ypos: 2
            xpos: $xa2
            # rpos: $xa2
            zpos: $za2
            pitch: $pita2
            yaw: $yawa2
            

        detector: 
        - name: det0
            xpos: $xdet
            ypos: 0
            zpos: $zdet
            pitch: $pitdet
            yaw: 0

"""
