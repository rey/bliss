# Installation and configuration of XIA MCA

Installation of XIA devices is common to the 3 XIA MCA electronics:

* Xmap
* Mercury
* FalconX

Devices are plugged in a windows (10 pro 64 bits) computer.

BLISS must be installed on the windows PC to be able to run a BLISS RPC server.

**Handel** is the library used to deal with XIA devices and data. Handel comes
with **ProSpect** which is the windows software provided by XIA to test and
calibrate XIA devices.

To access XIA device using the Handel library from BLISS running on a linux
station, a BLISS rpc server named `bliss-handel-server` must be running on the
windows PC.


There are 2 versions of ProSpect:

* ProSpect for Xmap and Mercury (merged with the deprecated *xManager*)
* ProSpect for FalconX


## Windows PC installation

!!! note "Windows version must be 10 pro 64 bits"
    Windows 7 is now deprecated.

### Installation of conda

* Download a conda installer:
    * [miniconda](https://repo.anaconda.com/miniconda/Miniconda3-latest-Windows-x86_64.exe)
* Launch installer:
    * `Next`
    * `I agree`
    * tick "All users"
    * tick "use as default python"
    * "Destination Folder:" `C:\ProgramData\Miniconda3` / `Next`
    * `Install`
    * untick "Anacoda Individual Edition Tutorial" +  "Getting Started with Anaconda" / `Finish`
* Create a link from taskbar to anaconda shell
    *  click Windows Start Menu / type anaconda / highlight "Anaconda Powershell Prompt " + select " > Pin to taskbar" on the right panel
* Create `bliss` Conda environment with python 3.7 and git
    * Start anaconda shell
    * `conda create -n bliss -y python=3.7 git pip`
* Activate bliss environment: `conda activate bliss`
* Install git support for python: `conda install -y gitpython` (2021-05: V3.1.11)
* Configue channels:

```
conda config --env --set channel_priority false
conda config --env --add channels conda-forge
conda config --env --append channels defaults
conda config --env --append channels tango-controls
conda config --env --append channels esrf-bcu
```

### Installation of BLISS

* clone and install BLISS
    ```
    git clone https://gitlab.esrf.fr/bliss/bliss bliss.git
    
    cd bliss.git
    
    git checkout 1.8.x   # <--- choose your branch
    
    conda install --file requirements-win64.txt
    
    pip install -e . --no-deps
    ```

* test bliss installation:
    ```
    C:\ python
    >>> import bliss
    >>>
    ```


### Installation of XIA software

* Copy into `c:\blissadm` files an directories found in:

```
P:\\ISDD\SOFTWARE\Public\distrib\XIA\
```

!!!note "Nov. 2021 versions"

    * *ProSpect* for Falconx
        * version 1.1.62
    * *ProSpect* for Mercury and Xmap
        * version 1.1.61
    * *Handel-all* library for Mercury/Xmap devices
        * version 1.2.28 64 bits
    * *Handel-sitoro* library for Falconx
        * version 1.1.22 64 bits


* Depending on the XIA device:
    * Install the corresponding *ProSpect*
    * Test connection to the device with *ProSpect*

!!! note "For XMAP"
    PXI-PCI bus-coupler cards must be installed. see:
    http://wikiserv.esrf.fr/bliss/index.php/XIA_Electronics_Installation#XMAP_installation.2Fupgrade_on_windows_7_64_bits

!!! note "For FalconX"
    * Connection to the falconX can be tested with a browser using address `http://192.168.200.201`
    * Check firware version (nov. 2021: `21.2.0`)




### Server Startup script

Depending on device, put `falconx\falconx-start` or `xmap\xmap-start` shortcut
on the desktop.

These shortcuts are pointing on `falconx\falconx-server.ps1` or
`xmap\xmap-server.ps1` that have to be customized according to Beamline needs.






### Developer's details

`bliss-handel-server` start-up script is created at installation using
the **entry_points** definitions in `setup.py` of BLISS repository.

```python
entry_points={
     "console_scripts": [
          ...
          "bliss-handel-server = bliss.controllers.mca.handel.server:main",
          ] }
```

The wrapping of the Handel library is made with cffi. see:
`bliss/controllers/mca/handel/_cffi.py`



## Configuration in BLISS


Example for mercury:
```yaml
- name: mercury
  module: mca
  class: Mercury
  url: tcp://wfamexia:8000
  configuration_directory: C:\\blissadm\\mercury\\config\\BM16
  default_configuration: Vortex3_Mercury4ch_05us_Hg.ini
```

Example for FalconX:
```yaml
- name: fxid42
  module: mca
  class: FalconX
  url: tcp://wid421:8000
  configuration_directory: C:\\blissadm\\falconx\\config\\ID42
  default_configuration: falconxn.ini
```

Example for Xmap:
```yaml
- name: fxid16
  module: mca
  class: XMAP
  url: tcp://wid421:8000
  configuration_directory: C:\\blissadm\\falconx\\config\\ID42
  default_configuration: xmap.ini

```

## NOTES

Python script to parse binary mapping data
http://support.xia.com/default.asp?W882


