# 2D detectors (with Lima) #


2D detectors (CCD, pixel, ...) supported by Lima can be controlled in BLISS via the BLISS `Lima` class.

This class uses the usual Lima Tango device server to access the detector.


## Configuration ##

    beamline_configuration/
    ├── eh2
    │   ├── cameras
    │   │   ├── andor1.yml
    │   │   ├── andor2.yml
    │   │   ├── cdte22.yml
    │   │   ├── eiger1.yml
    │   │   └── mpx22.yml

Example of YAML configuration file:

    % cat mpx22.yml
       name: mpx22
       class: Lima
       tango_url: id42/limaccd/mpx_22
       tango_timeout: 120

### Additional parameters

Each individual Lima sub-device can be configured directly from the YML file.
The section names are the sub-devices names, item keys are the attribute names
just like specified in the Tango device:

#### Saving example

    saving:
        mode: ONE_FILE_PER_FRAME
        file_format: EDF
        _max_writing_tasks: 4

#### Processing example

    processing:
        runlevel_roicounter: 9

#### Image example

    image:
        rotation: 90

#### Accumulation example
  
    accumulation:
        max_expo_time: 2

### Directories mapping

Directories mapping allows to replace filesystem paths used by BLISS, in favour of different
mappings when Lima is running under Windows for example or on a computer with different
mounting points.

Paths specified in `SCAN_SAVING` will be transformed, in order to pass the good path arguments
to Lima:

    directories_mapping:
        default:
            - path: /data/visitor
              replace-with: Z:/visitor
            - path: /data/id15
              replace-with: Z:/id15

### Configuring scan default chain with Lima devices

Default chain configuration can have an `acquisition_settings` section, which allows to
set attributes on the main Lima Tango device at scan preparation time. The item key names
correspond to the Tango device attributes.

#### Setting 2D detectors in external trigger mode only
    
    - name: default_chain_eh2
      plugin: default
      chain_config:
        - device: $mpx22
          acquisition_settings:
            acq_trigger_mode: EXTERNAL_TRIGGER_MULTI
          master: $p201_20

## Miscellaneous

### Taking dark (background) images

The `.take_dark()` method of Lima controller objects can be used to take background images,
that will be substracted to subsequent frames during acquisition, in order to reduce
noise level.

`.take_dark` accepts the following arguments:

- exposure time (in s.)
- optional arguments:
    - shutter, `None` by default
    - save [bool], `True` by default

When `save` is True, current SCAN_SAVING parameters are used to save the background image,
and to install it as Lima reference for background substraction. Currently background
images are always saved in EDF format. A `background_` prefix is added to the image prefix.
If `save` is False, an image is taken but it only stays in memory ; Lima will use this
internal image to do background substraction.

The shutter argument allows to pass a BLISS `Shutter` object, that will be **closed**
while taking the background image. If the shutter was open when passed to the function,
it will be re-opened at the end of the `take_dark` procedure.

### Example to add a pseudo axis to drive the Maxipix's threshold

    if hasattr(setup_globals,'mpx22'):
        mpxthl = SoftAxis('mpxthl', mpx22.camera,
                           position='energy_threshold',
                           move='energy_threshold')

