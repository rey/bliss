# Monochromators

## Classes

BLISS provides classes which represent a set of common functionalities for monochromator control.

We assume that a monochromator is composed of:

- Rotation motor (bragg angle - real motor)
- Energy motor (Calc Motor)
- Crystal(s)
- Energy tracking object (how motors should adjust for a given energy)

This is represented by 4 classes `MonochromatorBase`, `XtalManager`, `EnergyCalcMotor` and `EnergyTrackingObject`.

### `MonochromatorBase`

This is the base class for monochromators.

### Fix Exit

* For DCM monochromator, a fix exit offset can be given and passed
    to the XtalManager object (see XTalManager section)
    - YAML file

        ```yaml
        - package: bm23.MONO.BM23mono
          class: BM23Mono
          name: bm23mono
          xtals: $bm23_mono_xtals
          energy_motor: $bm23energy
          bragg_motor: $bm23bragg
          fix_exit_offset: 25.0     <--
        ```

* If the monochromator holds a set of crystals, a method to move
    from one crystal to the other(s) is foreseen.
    In this case, a new class should be written which inherits
    from MonochromatorBase Class.
    - 3 methods should be re-written:
        + initialize(self)
            Allow to get the necessary parameters in the mono yaml file
        + _xtal_change(self, xtal)
            Move "xtal" in the beam
        + _xtal_is_in(self, xtal)
            return True if "xtal" is in the beam, False otherwise
    - Additional parameters should also be added in the XtalManager
        YAML file to get, for example the position of the crystals in the beam

    - Example:
        + Mono YAML file example

            ```yaml
            - plugin: bliss
              package: bm23.MONO.BM23mono
              class: BM23Mono
              name: bm23mono
              xtals: $bm23_mono_xtals
              energy_motor: $bm23energy
              bragg_motor: $bm23bragg
              change_motor: $smty
              fix_exit_offset: 25.0
            ```

### `XtalManager`

`XtalManager` describes the crystal(s) mounted on the monochromator.

* At least one crystal need to be defined ...
    - YAML file:

        ```yaml
        - plugin: bliss
            module: monochromator
            class: XtalManager
            name: mono_xtals
            xtals:
                - xtal: Si220
        ```

    - Available method:
        + bragg2energy
        + energy2bragg
* ... but more may be used:
    - YAML file:

        ```yaml
        xtals:
            - xtal: Si111
            - xtal: Si311  <--
            - xtal: Ge511  <--
        ```

* More methods are available for DCM monochromator,
    if the "fix_exit_offset" attribute is set:
        + bragg2dxtal
        + energy2dxtal
        + dxtal2bragg
    - YAML file:

        ```yaml
        plugin: bliss
        package: bm23.MONO.BM23monochromator
        class: XtalManager
        name: bm23_mono_xtals
        fix_exit_offset: -10    <--
        xtals:
            - xtal: Si111
            - xtal: Si311
            - xtal: Ge511
        ```

* If you want to use crystal harmonics, add a crystal with its
    corresponding HKL values. You will be able to use it by changing
    the selected crystal with the Monochromator object (mono.xtal_change(new_xtal)).
    - YAML file:

        ```yaml
        xtals:
            - xtal: Si111
            - xtal: Si333  -> Use third harmonic of Si111
            - xtal: Si311
            - xtal: Ge511  
        ```

* Calculations are using the module bliss.physics.diffraction based
    on the public module mendeleev which define a number of crystals parameters.
    You may specify your own dspacing (in Angstrom) in the yml file.
    In this case, this value MUST be specified for each crystal
    - YAML file:

        ```yaml
        xtals:
            - xtal: Si111
                dspacing: 3.1356  --> new dspacing for Si111
            - xtal: Si333
                dspacing: 3.1356  --> need to be set to be coherent
            - xtal: Ge511         --> using default constants
        ```

* Multilayers:
    + Principle
        - the Bragg to Energy calculation for a multilayer is given by:
        n*lambda = 2*d-spacing*sqrt(1-2*delta_bar/pow(sin(theta),2))*sin(theta)
        - n = order of reflexion
        - d-spacing of such a multilayer is the sum of the thickness
        of each material (in Angstrom in the calculation)
        - delta_bar:
        . Parameter which is energy dependant.
        . Given by the Optic Group as a file Energy/Delta_bar
        . this formula is not bijective. In consequence a lookup table
            is built at the creation of the object to get energy
            from angle or angle from energy.

    + Configuration
        + to define a multilayer, use the tag "multilayer" instead of "xtal"
        + dpspacing:
            . Use tag "thickness1" and "thickness2" if the materials and their
            thicknesses are known. Only 2 materials are taken into account.
            Thickness must be given in nm
            . Use tag "dspacing" if the materials are unknown but you know
            an approximate dspacing.
            dspacing must be given in nm
        + delta_bar:
            . Use tag "delta_bar" to specify the file given by the optic group
            . if delta_bar is not specified, delta_bar=0
        + lookup table:
            . if none of the parameters are known you can directly specify a
            lookup table energy(eV) vs bragg angle (radian)
            . Use the tag "lookup_table" to specify the file containing the lut

    + YAML File:

        ```yaml
        xtals:
        - xtal: Si111       <-- Normal crystal definition

        - multilayer: ML_1  <-- Multilayer: All parameters are known
          dspacing1: 10        
          dspacing1: 11    
          delta_bar: /users/blissadm/local/beamline_configuration/mono/multilayer/OpticalConstantsW_B4C.txt

        - multilayer: ML_2  <-- Multilayer: Only an approximate dspacing is known
          dspacing: 21                      delta_bar = 0

        - multilayer: ML_2  <-- Multilayer: Only a Energy<-> Bragg lookup table is given
          lookup_table: /users/blissadm/local/beamline_configuration/mono/multilayer/Bm29_multilayer.txt
        ```

### Generic plugin & subitems

The monochromator object has two sub objects, an energy axis controlled by a `EnergyCalcMotor` and an energy tracking axis controlled by a `EnergyTrackingCalcMotor`.
For convenience, you don't have to write the full yaml configuration for these objects. It will be generated and created on the fly by the monochromator object during it's instanciation.

This mechanism uses the new generic plugin of BLISS. Thus both subitems can be retrieved separately by `config.get()` or added into a session like any other BLISS object.

!!! note "Customize the class of the energy calc controller"
    It's possible to specify the class of the calc controller to be used with the `ctrl_class` key.

    Example:
    ```yaml
      energy_motor:
        ctrl_class: path.to.module.MyEnergyCalcMotor
        name: ene
        unit: keV
      energy_track_motor:
        ctrl_class: path.to.module.MyEnergyTrackingCalcMotor
        name: enetrack
        approximation: 0.0005
        unit: keV
    ```


#### `EnergyCalcMotor`

`EnergyCalcMotor` and `EnergyTrackingCalcMotor` need to know the Monochromator it is refering to.
At Initialization of the mono, the method EnergyCalcMotor.set_mono(self)
is called.
The Value of the EnergyCalcMotor is Nan before this call or if no crystal
is selected in the XtalManager object. The Monochromator object
is in charge to do this selection.

```yaml
# Energy Calc Motor config is like:
- plugin: emotion
  module: monochromator
  class: EnergyCalcMotor
  axes:
    - name: $sim_mono
      tags: real bragg
    - name: ene
      tags: energy
      unit: keV
```

#### `EnergyTrackingCalcMotor`

```yaml
# EnergyTrack CalcMotor config is like:

- plugin: emotion
  module: monochromator
  class: EnergyTrackingCalcMotor
  approximation: 0.0005
  axes:
    - name: $ene
      tags: real energy
    - name: $sim_c1
      tags: real sim_c1
    - name: $sim_c2
      tags: real sim_c2
    - name: $sim_acc
      tags: real sim_acc
    - name: enetrack
      tags: energy_track
      unit: keV
```

### `EnergyTrackingObject`

The monochromator can have several `EnergyTrackingObject`. It contains the list of axes that require to adjust its position when the bragg angle (and the energy) changes.

Each motor can have one or several trajectories (or harmonics) defined.

Each trajectory can have one or several tracking modes defined. See details below.

## Configuration

```yaml
# Monochromator
- plugin: generic           # or 'bliss_controller' for bliss 1.8
  module: monochromator
  class: MonochromatorBase
  name: mono
  xtals: $mono_xtals        # the crystals manager
  bragg_motor: $bragg       # the bragg angle axis
  energy_motor:             # the energy axis subitem
    name: ene               # controller class can be customized with `ctrl_class`
    unit: keV
  energy_track_motor:       # the energy track axis subitem
    name: enetrack          # controller class can be customized with `ctrl_class
    approximation: 0.0005
    unit: keV
  trackers:                 # a list of tracking objects
    - $focus_tracker
    - $sample_tracker

# Xtals Manager
- plugin: bliss
  module: monochromator
  class: XtalManager
  name: mono_xtals
  xtals:
    - xtal: Si220
      dspacing: 1.920171445282458
    - xtal: Si311
      dspacing: 3.141592653589793

# Tracking objects
- plugin: bliss
  module: monochromator
  class: EnergyTrackingObject
  name: focus_tracker
  tracking:
    - motor: $sim_c1
      mode: table
      parameters:
        - trajectory: 1
          table: "monochromator/focus_C1.dat"
        - trajectory: 2
          table: "monochromator/focus_C1.dat"
          polynom:
            E6: -2.305918e-07
            E5: 1.772031e-05
            E4: -0.0005460834
            E3: 0.008611169
            E2: -0.07324875
            E1: 0.3891596
            E0: -0.4258383
          theory:
            TG0: 0.1
            TL0: 0.2
            TGAM: 0.3
            TB0: 0.4
    - motor: $sim_c2
      mode: polynom
      parameters:
        - trajectory: 1
          table: "monochromator/focus_C2.dat"
          polynom:
            E6: -2.305918e-07
            E5: 1.772031e-05
            E4: -0.0005460834
            E3: 0.008611169
            E2: -0.07324875
            E1: 0.3891596
            E0: -0.4258383
- plugin: bliss
  module: monochromator
  class: EnergyTrackingObject
  name: sample_tracker
  tracking:
    - motor: $sim_acc
      mode: table
      parameters:
        - trajectory: default
          table: "monochromator/focus_ACC.dat"
        - trajectory: alt
          table: "monochromator/focus_ACC_alt.dat"
```

## Usage

### Monochromator information

```python
MONO_SESSION [15]: mono
         Out [15]: Monochromator: mono
                   
                   Crystal: Si220 [Si220 / Si311]
                   dspacing: 1.92017 angstrom
                   
                                    bragg         ene    enetrack
                   Calculated  10.309 deg  18.041 keV  18.041 keV
                      Current  10.309 deg  18.041 keV  18.041 keV
                   
                   FOCUS_TRACKER
                                 sim_c1    sim_c2
                   Calculated  1.388 mm  1.322 mm
                      Current  1.388 mm  1.322 mm
                     Tracking        ON        ON
                   
                   SAMPLE_TRACKER
                                 sim_acc
                   Calculated  -0.106 mm
                      Current   0.000 mm
                     Tracking        OFF
```

Display information about:

- selected crystal
- bragg angle and corresponding energy
- status of tracked motors
  - current position
  - calculated trajectory position given tracking parameters and current energy
  - tracking status (ON or OFF)

### Motor tracking information

Current tracking info is given by `track_info` method of monochromator object or tracking object.

```python
MONO_SESSION [16]: mono.track_info()

  FOCUS_TRACKER

              sim_c1    sim_c2
  Tracking        ON        ON
      Mode     table     table
Trajectory         1         1
  On traj.       YES       YES

  SAMPLE_TRACKER

              sim_acc
  Tracking        OFF
      Mode      table
Trajectory    default
  On traj.         NO
```

For each tracker object and motor, display information about:

- tracking status (ON or OFF)
- tracking mode (table / polynom / theory)
- selected trajectory (or harmonic for undulators)
- weither motor is on trajectory or not (YES / NO)

### Toggle tracking state

Use `track_on` / `track_off` methods to enable or disable tracking on tracking objects, or `track` boolean property on
motors directly.

```python
# Enable tracking for monochromator (all trackers and motors)
MONO_SESSION [1]: mono.track_on()

# Disable tracking for a tracker object only
MONO_SESSION [2]: mono.tracker.focus_tracker.track_off()

# Get axis current tracking state
MONO_SESSION [3]: myaxis.track
         Out [3]: False

# Enable tracking on axis object
MONO_SESSION [4]: myaxis.track = True
```

### Tracking mode

Energy can be tracked using several modes `table`, `polynom` or `theory`.

* `table` uses a calibration file (see [track tables](#track-tables) below)
* `polynom` uses the polynom defined by its coefficients from `E0` to `E6`
* `theory` uses a formula for undulators (gap) defined by `TG0`, `TL0`, `TGAM`, `TB0`.

Configuration can specify one or several modes for each trajectory in parameters. The default mode is given by the `mode` specified in yaml file.

The current mode can be set with `track_mode` property.

```python
# Set tracking mode on axis object via monochromator object
MONO_SESSION [1]: mono.track_mode(myaxis)
         Out [1]: 'polynom'

# Set tracking mode on axis object via monochromator object
MONO_SESSION [2]: mono.track_mode(myaxis, "table")

# Get current tracking mode of an axis object
MONO_SESSION [3]: myaxis.track_mode
         Out [3]: 'table'

# Set current tracking mode of an axis object
MONO_SESSION [4]: myaxis.track_mode = 'polynom'
```

### Switch trajectories or harmonics

Tracking parameters for each motor can specify different settings for different trajectories.

Each trajectory must have parameters defined for at least one mode (table, polynom or theory).

!!! note "Undulator harmonics"
    In case of undulator monochromator, the trajectories represent the different harmonics.
    
    The keywork `harmonic` can be used in place of `trajectory` in yaml configuration.
    
    The property `harmonic` also exists on trackers and axes objects, and can be used for convenience in place of `trajectory` property (both exist).

```python
# Get current trajectory name
MONO_SESSION [1]: sim_c1.trajectory
         Out [1]: 1

# Switch to 2nd trajectory (name could be a string in yaml)
MONO_SESSION [2]: sim_c1.trajectory = 2

# note: sim_c1.harmonic works as well
```

### Track tables

Tracking tables allow a motor position to be calibrated to a given energy point. The table define a trajectory that the motor will track when the energy changes.

It relies on [XCalibu](https://github.com/cguilloud/xcalibu) project (for tables only).

File format is a 2 columns text file with energy and motor position for each point.

```text
# Energy motor_position
10.0000  1.1514
11.0000  1.3532
...      ...
```

Between two energy points, the position of the motor is interpolated using linear regression.

**Usage**

`track_tables` property of monochromator object allow to interact with tracking tables.

It allows to print, plot and modify tables with methods `setpoint`, `delpoint`, `save`, `plot`.

```python
# Display current tables for all currently tracked motors
MONO_SESSION [1]: mono.track_tables
         Out [1]: 
                    Energy    sim_c1    sim_c2    sim_acc
                  --------  --------  --------  ---------
                    4.9399  0.470157  0.411799  -0.146702
                    5       0.475166  0.416808  -0.145901
                    5.5     0.51492   0.449367  -0.141491
                    5.7503  0.533636  0.466079  -0.138115
                    5.9996  0.552033  0.482473  -0.136984
                    6.5     0.588508  0.516125  -0.133978
                    6.9999  0.621568  0.552008  -0.13299
                    7.5004  0.657133  0.585569  -0.130494
                    7.9998  0.692698  0.621134  -0.128493
                    8.5001  0.725257  0.659704  -0.126993
                    8.9996  0.761323  0.692263  -0.124993
                    9.4979  0.794383  0.726826  -0.123493
                    ...

# Add current point (energy, motor position) in tables for all tracked motors
MONO_SESSION [2]: mono.track_tables.setpoint()

# Delete energy point from tables for all currently tracked motors
MONO_SESSION [3]: mono.track_tables.delpoint()     # current energy point
MONO_SESSION [4]: mono.track_tables.delpoint(6.5)  # delete 6.5 kev point

# Save modified table files to disk (in beamline configuration)
MONO_SESSION [5]: mono.track_tables.save()

# Display trajectory in a plot (for given axis)
MONO_SESSION [6]: mono.track_tables.plot("axis")
```

`track_table` property of axis or tracking object allow do the same on a per axis basis.

```python
# Display current table for a tracked motor
MONO_SESSION [1]: sim_c1.track_table
         Out [1]: 
                    Energy    sim_c1
                  --------  --------
                    4.9399  0.470157
                    5       0.475166
                    5.5     0.51492
                    5.7503  0.533636
                    5.9996  0.552033
                    6.5     0.588508
                    6.9999  0.621568
                    7.5004  0.657133
                    7.9998  0.692698
                    8.5001  0.725257
                    8.9996  0.761323
                    9.4979  0.794383
                    ...

# Add current point (energy, motor position) in table
MONO_SESSION [2]: sim_c1.track_table.setpoint()

# Delete energy point from table
MONO_SESSION [3]: sim_c1.track_table.delpoint()     # current energy point
MONO_SESSION [4]: sim_c1.track_table.delpoint(6.5)  # delete 6.5 kev point

# Save modified table files to disk (in beamline configuration)
MONO_SESSION [5]: sim_c1.track_tables.save()

# Display trajectory in a plot
MONO_SESSION [6]: sim_c1.track_tables.plot()
```

Note: a **tolerance** of 10 eV (by default) is applied when replacing/deleting an enegy point in the table. It's
configurable with `sim_c1.track_tables._kev_tolerance`.

Access to underlying **XCalibu object** is given by `sim_c1.track_table.calib` object for lower level operations.

!!! note "Backup when saving tables"
    When saving changes with `track_table.save()` the old table file is backed up with the date in the filename extension,
    (up to one backup per day).
