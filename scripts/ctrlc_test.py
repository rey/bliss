import bliss
from bliss.config import static
import gevent
import os
import sys
import signal
import argparse
import subprocess
from bliss.common.tango import DeviceProxy
from bliss.common.scans import loopscan


def register_wdnhandler():
    old_handler = signal.signal(signal.SIGINT)

    def new_handler(*args):
        print("WDN HANDLER")
        old_handler(*args)

    signal.signal(signal.SIGINT, new_handler)


def child():
    try:
        print("STARTED")
        gevent.sleep(100)
    except BaseException as e:
        print("UNEXPECTED EXCEPTION IN CHILD", type(e))


def parent1():
    try:
        gevent.spawn(child).get()
    except BaseException as e:
        print("UNEXPECTED EXCEPTION IN PARENT1", type(e))


def parent2():
    try:
        gevent.spawn(parent1).get()
    except BaseException as e:
        print("UNEXPECTED EXCEPTION IN PARENT2", type(e))


def main_simple():
    os.environ["TANGO_HOST"] = "localhost:10000"
    device = DeviceProxy("id00/limaccds/simulator1")
    device.status()
    try:
        gevent.spawn(parent2).join()
    except KeyboardInterrupt as e:
        print("EXPECTED CTRL_C IN MAIN")
    except BaseException as e:
        print("UNEXPECTED EXCEPTION IN MAIN", type(e))


def main_bliss():
    os.environ["BEACON_HOST"] = "localhost:10001"
    os.environ["TANGO_HOST"] = "localhost:10000"
    config = static.get_config()
    bliss_session = config.get("demo_session")
    assert bliss_session.setup(), "Session setup failed"
    lima_simulator = config.get("lima_simulator")
    current_eval_g = (
        None
    )  # greenlet to simulate command being executed in the shell, in order to do testing of proposed fix for #2921

    def handle_ctrlc():
        if current_eval_g:
            current_eval_g.kill(KeyboardInterrupt)

    # use signal handler via libev, plays more nicely with the loop
    sigint_handler = gevent.signal_handler(signal.SIGINT, handle_ctrlc)

    print("STARTED")
    # start greenlet for loopscan, just like a user would do by typing the command and pressing Enter in shell
    # (in accordance with proposed fix for #2921)
    current_eval_g = gevent.spawn(loopscan, 10000, 0.001, lima_simulator, save=False)
    # wait for end of evaluation => this will get interrupted by SIGINT from external process
    current_eval_g.get()
    # NOTE: capture KeyBoardInterrupt in the Bliss code and print("UNEXPECTED ...")


def test_subprocess(with_bliss):
    if with_bliss:
        args = [sys.executable, __file__, "--subprocess", "--bliss"]
        sleep = 5
    else:
        args = [sys.executable, __file__, "--subprocess"]
        sleep = 1
    proc = subprocess.Popen(args, stdout=subprocess.PIPE)
    gevent.sleep(sleep)
    proc.send_signal(signal.SIGINT)
    stdout, _ = proc.communicate()
    print("\n")
    print(stdout.decode())
    for line in stdout.decode().split("\n"):
        if "UNEXPECTED" in line:
            raise RuntimeError("UNEXPECTED EXCEPTION IN NON-MAIN GREENLET")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--subprocess", action="store_true")
    parser.add_argument("--bliss", action="store_true")
    args = parser.parse_args()
    if args.subprocess:
        if args.bliss:
            main_bliss()
        else:
            main_simple()
    else:
        while True:
            test_subprocess(args.bliss)

# HOW TO RUN THE TEST WITH --bliss OPTION:
#
# add try...except KeyboardInterrupt around '_run_next' in bliss/scanning/scan.py
#
# try:
#    ...
# except KeyboardInterrupt:
#     print("UNEXPECTED KeyboardInterrupt")
#     raise
#
# add try...except KeyboardInterrupt before channel emit (line 453) in bliss/scanning/channel.py
# with UNEXPECTED KeyboardInterrupt message
# add try...except KeyboardInterrupt around 'emit' in bliss/scanning/channel.py
